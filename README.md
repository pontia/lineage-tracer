Software tools for cell tracking and lineage reconstruction in 2D microbial cell cultures.

Requires:

- python >3.5
- pandas
- numpy
- scipy
- pyqt=5
- h5py
- Cython (optional)
- unittest (optional)
- mkl
