%% Goal of this script is to generate a concatenated file from the CellX outputs. 
% Input: CellX txt files single frames
% Output: matlab structure with concatenated content of CellX output files.
% Outputname = s
% problem - header of the cellx output is diverse and not accounted for in
% the code. In the current version this has to be done manually

%% Initialize variables.
% start frame number
j=1;
% how many of the files do you want to use - change accordingly
endframe = 100;
% path to the folder containing segmentation results change accordingly
% path = 'C:\Users\ursku\Desktop\MovieForUrs\CellX_Position010100\Tea200_position010100_time\'; % ORIGINAL PATH
path = 'F:\Data\Projects\fabian\TrackingLineage_from_Fabian\well A16\ora_red_yel\';

% filename deconstruction from cellx
filename = strcat(path,'cells_', num2str(j,'%05d'),'.txt'); 
delimiter = '\t';
startRow = 2;

%% Format string for each line of text:
% Matlab specific construction of a txt file
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);

%% Close the text file.
fclose(fileID);
%% 
% header of CellX file ... needs to be changed manually if necessary
s = struct('cellframe',dataArray{:, 1},'cellindex',dataArray{:, 2},'cellcenterx',dataArray{:, 3},'cellcentery',dataArray{:, 4},...
'cellmajoraxis',dataArray{:, 5},'cellminoraxis',dataArray{:, 6},'cellorientation',dataArray{:, 7},'cellarea',dataArray{:, 8},...
'cellvolume',dataArray{:, 9},'cellperimeter',dataArray{:, 10},'celleccentricity', dataArray{:, 11},'cellfractionOfGoodMembranePixels', dataArray{:, 12},...
'cellmemarea',dataArray{:, 13},'cellmemvolume',dataArray{:, 14},'cellnucradius',dataArray{:, 15},'cellnucarea',dataArray{:, 16},...
'Tea200_position010100_timebackgroundmean',dataArray{:, 17},'Tea200_position010100_timebackgroundstd', dataArray{:, 18},'Tea200_position010100_timecelltotal', dataArray{:, 19},...
'Tea200_position010100_timecellq25', dataArray{:, 20},'Tea200_position010100_timecellq50', dataArray{:, 21},'Tea200_position010100_timecellq75', dataArray{:, 22},...
'Tea200_position010100_timememtotal', dataArray{:, 23},'Tea200_position010100_timememq25' , dataArray{:, 24},'Tea200_position010100_timememq50' , dataArray{:, 25},...
'Tea200_position010100_timememq75' , dataArray{:, 26},'Tea200_position010100_timenuctotal', dataArray{:, 27},'Tea200_position010100_timenucq25' , dataArray{:, 28},...
'Tea200_position010100_timenucq50', dataArray{:, 29},'Tea200_position010100_timenucq75' , dataArray{:, 30},'Tea200_position010100_timebrighttotal' , dataArray{:, 31},...
'Tea200_position010100_timebrightq25' , dataArray{:, 32},'Tea200_position010100_timebrightq50' , dataArray{:, 33},'Tea200_position010100_timebrightq75' , dataArray{:, 34},...
'Tea200_position010100_timebrighteuler' , dataArray{:, 35});
j=2;

% while loop over all txt files from the cellx output
while j <= endframe
%% Initialize variables.
filename = strcat(path,'cells_', num2str(j,'%05d'),'.txt'); % change
delimiter = '\t';
startRow = 2;

%% Format string for each line of text:
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';
%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);

%% Close the text file.
fclose(fileID);
%%
s1 = struct('cellframe',dataArray{:, 1},'cellindex',dataArray{:, 2},'cellcenterx',dataArray{:, 3},'cellcentery',dataArray{:, 4},...
'cellmajoraxis',dataArray{:, 5},'cellminoraxis',dataArray{:, 6},'cellorientation',dataArray{:, 7},'cellarea',dataArray{:, 8},...
'cellvolume',dataArray{:, 9},'cellperimeter',dataArray{:, 10},'celleccentricity', dataArray{:, 11},'cellfractionOfGoodMembranePixels', dataArray{:, 12},...
'cellmemarea',dataArray{:, 13},'cellmemvolume',dataArray{:, 14},'cellnucradius',dataArray{:, 15},'cellnucarea',dataArray{:, 16},...
'Tea200_position010100_timebackgroundmean',dataArray{:, 17},'Tea200_position010100_timebackgroundstd', dataArray{:, 18},'Tea200_position010100_timecelltotal', dataArray{:, 19},...
'Tea200_position010100_timecellq25', dataArray{:, 20},'Tea200_position010100_timecellq50', dataArray{:, 21},'Tea200_position010100_timecellq75', dataArray{:, 22},...
'Tea200_position010100_timememtotal', dataArray{:, 23},'Tea200_position010100_timememq25' , dataArray{:, 24},'Tea200_position010100_timememq50' , dataArray{:, 25},...
'Tea200_position010100_timememq75' , dataArray{:, 26},'Tea200_position010100_timenuctotal', dataArray{:, 27},'Tea200_position010100_timenucq25' , dataArray{:, 28},...
'Tea200_position010100_timenucq50', dataArray{:, 29},'Tea200_position010100_timenucq75' , dataArray{:, 30},'Tea200_position010100_timebrighttotal' , dataArray{:, 31},...
'Tea200_position010100_timebrightq25' , dataArray{:, 32},'Tea200_position010100_timebrightq50' , dataArray{:, 33},'Tea200_position010100_timebrightq75' , dataArray{:, 34},...
'Tea200_position010100_timebrighteuler' , dataArray{:, 35});

%% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray ans;
%%
mynames= fieldnames(s);
for i = 1:numel(mynames)
s.(mynames{i})=[s.(mynames{i}); s1.(mynames{i})];
end
j = j+1;
end
%%
clearvars i j s1;