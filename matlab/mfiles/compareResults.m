function compareResults(pythonResultDir, matlabResultDir)

if nargin == 0
    pythonResultDir = 'F:\Data\Projects\fabian\TrackingLineage_from_Fabian\results_lapjv2';
    matlabResultDir = 'F:\Data\Projects\fabian\TrackingLineage_from_Fabian\results_matlab';
end

strg = sprintf('%%.%dd', 5);

found = 1;
i = 0;
while found == 1

    i = i + 1;
    try
        warning off
        pythonFile = readtable(fullfile(pythonResultDir, ['cells_', sprintf(strg, i), '.txt']), 'Delimiter', '\t');
        matlabFile = readtable(fullfile(matlabResultDir, ['cells_', sprintf(strg, i), '.txt']), 'Delimiter', '\t');
        warning on

        % Track index
        pythonResultsTrackIndex = pythonFile.track_index + 1;
        matlabResultsTrackIndex = matlabFile.Var1;

        cmpTrackIndex = all(pythonResultsTrackIndex == matlabResultsTrackIndex);

        % Cost
        costErrors = [];
        if ismember('cost', pythonFile.Properties.VariableNames)

            % Little trick to compare with nan

            % Cost
            assert(all(find(isnan(pythonFile.cost)) == find(isnan(matlabFile.Var2))));

            pythonResultsCost = pythonFile.cost;
            pythonResultsCost(isnan(pythonResultsCost)) = 1000.0;

            matlabResultsCost = matlabFile.Var2;
            matlabResultsCost(isnan(matlabResultsCost)) = 1000.0;

            % Max error in Cost
            maxErrCost = max(abs(pythonResultsCost - matlabResultsCost));

            % Cost 1
            pythonResultsCost1 = pythonFile.cost1;
            pythonResultsCost1(isnan(pythonResultsCost1)) = 1000.0;

            matlabResultsCost1 = matlabFile.Var3;
            matlabResultsCost1(isnan(matlabResultsCost1)) = 1000.0;

            % Max error in Cost1
            maxErrCost1 = max(abs(pythonResultsCost1 - matlabResultsCost1));


            % Cost 2
            pythonResultsCost2 = pythonFile.cost2;
            pythonResultsCost2(isnan(pythonResultsCost2)) = 1000.0;

            matlabResultsCost2 = matlabFile.Var4;
            matlabResultsCost2(isnan(matlabResultsCost2)) = 1000.0;

            % Max error in Cost2
            maxErrCost2 = max(abs(pythonResultsCost2 - matlabResultsCost2));

            % Cost 3
            pythonResultsCost3 = pythonFile.cost3;
            pythonResultsCost3(isnan(pythonResultsCost3)) = 1000.0;

            matlabResultsCost3 = matlabFile.Var5;
            matlabResultsCost3(isnan(matlabResultsCost3)) = 1000.0;

            % Max error in Cost3
            maxErrCost3 = max(abs(pythonResultsCost3 - matlabResultsCost3));

            % Cost 4
            pythonResultsCost4 = pythonFile.cost4;
            pythonResultsCost4(isnan(pythonResultsCost4)) = 1000.0;

            matlabResultsCost4 = matlabFile.Var6;
            matlabResultsCost4(isnan(matlabResultsCost4)) = 1000.0;

            % Max error in Cost4
            maxErrCost4 = max(abs(pythonResultsCost4 - matlabResultsCost4));

            costErrors = [maxErrCost, maxErrCost1, maxErrCost2, maxErrCost3, maxErrCost4];
        end

        errStr = '';
        if ~isempty(costErrors)
            errStr = sprintf(' Max abs errors = (%.8f, %.8f, %.8f, %.8f, %.8f)', costErrors);
        end

        if cmpTrackIndex == 1
            fprintf(1, ['cells_', sprintf(strg, i), '.txt: tracking results match! ', errStr, '\n']);
        else
            fprintf(1, ['* * * cells_', sprintf(strg, i), '.txt: results DO NOT MATCH! ', errStr, '\n']);
        end
    catch
        found = 0;
    end

end
