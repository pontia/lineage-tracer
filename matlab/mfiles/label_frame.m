function label_frame(A,B,tracks_in_fnumber,greyscale,frame_number,cmap,tracked)
blobMeasurements = regionprops(A);
numberOfBlobs = size(blobMeasurements, 1);

textFontSize = 5;	% Used to control size of "blob number" labels put atop the image.
labelShiftX = -10;	% Used to align the labels in the centers of the coins.
hold on;
d=(imfuse(greyscale,label2rgb(B,cmap),'blend'));
imshow(d,'Border','tight');
axis image; % Make sure image is not artificially stretched because of screen's aspect ratio.

allBlobCentroids = [blobMeasurements.Centroid];
centroidsX = allBlobCentroids(1:2:end-1);
centroidsY = allBlobCentroids(2:2:end);
cross = tracked.cost(tracked.cellframe==frame_number); %draw red crosses for high cost
for k = 1 : numberOfBlobs           % Loop through all blobs.
    nmb = tracks_in_fnumber(k);
%     if nmb > 50000
%         str = num2str(nmb);
%         str = str(end-1:end);
%     else
%     
%     end
    str = num2str(nmb);
    treshold = 1500;
    
    text(centroidsX(k) + labelShiftX, centroidsY(k), str, 'FontSize', textFontSize);
    if cross(k)> treshold;
        text(centroidsX(k) + labelShiftX, centroidsY(k), 'X', 'Color', 'red', 'FontSize', 15);
    end
%     if isempty(index{frame_number,k})==0
%     flickers1(k) = round(centroidsX(k));
%     flickers2(k) = round(centroidsY(k));
%     else
%     flickers1(k) = 0;
%     flickers2(k) = 0;
%     end
end
hold off;
%%
end