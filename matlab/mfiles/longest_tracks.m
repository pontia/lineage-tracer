function [count] = longest_tracks (trackindex)
x = unique(trackindex);
N = numel(x);
count = zeros(N,1);
for k = 1:N
    count(k) = sum(trackindex==x(k));
end
end