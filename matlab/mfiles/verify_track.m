track= 40;
frame_number= 1;
endframe= 25;
r = 50; % length of box

% folder of segmentation results change accordingly
path = 'C:\Users\ursku\Desktop\MovieForUrs\CellX_Position010100\Tea200_position010100_time\'; 

% folder of greyscale images
path_greyscale = 'C:\Users\ursku\Desktop\MovieForUrs\';
name_greyscale = 'BFdivide_position010100_time';
% opticalFlow = vision.OpticalFlow;
% opticalFlow.ReferenceFrameSource = 'Input port';
% opticalFlow.release();
% opticalFlow.OutputValue = 'Horizontal and vertical components in complex form';
%%
while frame_number < endframe
    disp(frame_number);
    k1= tracked.cellindex((tracked.cellframe==frame_number) & (tracked.trackindex==track));
    center1x= tracked.cellcenterx((tracked.cellframe==frame_number) & (tracked.trackindex==track)); 
    center1y= tracked.cellcentery((tracked.cellframe==frame_number) & (tracked.trackindex==track)); 
    j= 0;
    while isempty(k1) && j < endframe 
        j= j+1;
        k1= tracked.cellindex((tracked.cellframe==frame_number+j) & (tracked.trackindex==track));
        center1x= tracked.cellcenterx((tracked.cellframe==frame_number+j) & (tracked.trackindex==track)); 
        center1y= tracked.cellcentery((tracked.cellframe==frame_number+j) & (tracked.trackindex==track)); 
        
    end
%     file_name_path = strcat('C:\Users\ursku\Desktop\resultsCellX_new\mask_', num2str(frame_number, '%05d')); % path to mask
%     importmask(file_name_path);
%     A1= segmentationMask;
    image_to_read = strcat(path_greyscale, name_greyscale, num2str(frame_number, '%04d'),'.tif'); % path to greyscale image
    fr1 = imread(image_to_read);
%     blobMeasurements1 = regionprops(A1);
    
    frame_number= frame_number + j;
 
    k2= tracked.cellindex((tracked.cellframe==frame_number+1) & (tracked.trackindex==track));
    if isempty(k2)
    driftx=0;
    drifty=0;
    center2x= center1x + driftx;
    center2y= center1y + drifty;
    else        
    center2x= tracked.cellcenterx((tracked.cellframe==frame_number+1) & (tracked.trackindex==track));
    center2y= tracked.cellcentery((tracked.cellframe==frame_number+1) & (tracked.trackindex==track));
    end
    
    image_to_read = strcat(path_greyscale, name_greyscale, num2str(frame_number+1, '%04d'),'.tif'); % path to greyscale image
    fr2 = imread(image_to_read);
    
%%    
    crop1= [center1x-r,center1y-r, 2*r,2*r];
    crop2= [center2x-r,center2y-r, 2*r,2*r];
    
    i1 = imcrop(fr1,crop1); %fixed
    i2 = imcrop(fr2,crop2); %moving
    trf = imregcorr(i2,i1);
    Rfixed = imref2d(size(i1));
    movingReg = imwarp(i2,trf,'OutputView',Rfixed);
    %%
    F1 = fft2(i1);
    F2 = fft2(i2);
    F3 = fft2(movingReg);
    % Create phase difference matrix
    pdm = exp(1i*(angle(F1)-angle(F2)));
    % Solve for phase correlation function
    pcf = real(ifft2(pdm));
    pdm2 = exp(1i*(angle(F1)-angle(F3)));
    pcf2 = real(ifft2(pdm2));
    % K = imabsdiff(i1,movingReg);
    % [WARP, RHO] = iat_ecc(i1, i2);
    %%
    figure;
    subplot(3,3,1); imshow(i1)
    title(num2str(frame_number))
    subplot(3,3,2); imshow(i2)
    title(num2str(frame_number+1))
    subplot(3, 3, 3); imshowpair(i1,movingReg,'falsecolor');
    subplot(3, 3, 4); hist(pcf)
    subplot(3, 3, 5); hist(pcf2)
    subplot(3, 3, 6); imshowpair(i1,i2,'montage')
    subplot(3, 3, 7); imshowpair(i1,movingReg,'montage');
    subplot(3, 3, 8); imshowpair(i1,i2,'falsecolor');
    % subplot(3, 3, 9)
    % plot(diag(movingReg))
    % savefig(H,num2str(frame_number));
    % plot(pcf)%plot(ccr(:))
    % plot(ccr(:))
    
    frame_number= frame_number +1;
    
end
%%
%     while isempty(k2) && i<=4
%         i= i+1;
%         k2= tracked.cellindex((tracked.cellframe==frame_number+i) & (tracked.trackindex==track));
%         center2x= tracked.cellcenterx((tracked.cellframe==frame_number+i) & (tracked.trackindex==track));
%         center2y= tracked.cellcentery((tracked.cellframe==frame_number+i) & (tracked.trackindex==track));
%         
%     end
%     V= step(opticalFlow,double(fr1),double(fr2));
%     x = real(V);
%     y = imag(V);
