%% A function to visualize the individual tracks
% input = tracked cellx output, segmentation mask (mat file) and raw
% segmentation imgaes (greyscale images)
% output = 

% folder of segmentation results change accordingly
% path = 'C:\Users\ursku\Desktop\MovieForUrs\CellX_Position010100\Tea200_position010100_time\';  % ORIGINAL PATH
path = 'F:\Data\Projects\fabian\TrackingLineage_from_Fabian\ora_red_yel\';

% folder of greyscale images - raw images
% path_greyscale = 'C:\Users\ursku\Desktop\MovieForUrs\';  % ORIGINAL PATH
path_greyscale = 'F:\Data\Projects\fabian\TrackingLineage_from_Fabian\timelapse\';


% identifier of raw images
% name_greyscale = 'BFdivide_position010100_time';  % ORIGINAL NAME
name_greyscale = 'BF__position010100_time';

%%
% create 2 variables from tracked file
cellframe= tracked.cellframe;
trackindex= tracked.trackindex;
%%
%clf;
clearvars F stats;
%% define colormap
% and check that everything is unique
uv = unique(cellframe);
ncells = histc(cellframe,uv);
ncells_cumsum = [0 cumsum(ncells).'];
cmap = jet(150000);
cmap = cmap(randperm(150000),:);
%% 
% read images and color with color map
frame_number = 1;
endframe = 25;
for frame_number = 1:endframe
    file_name_path = strcat(path, 'mask_', num2str(frame_number, '%05d'));
    importmask(file_name_path);
    
    image_to_read = strcat(path_greyscale, name_greyscale, num2str(frame_number, '%04d'),'.tif'); 
    greyscale = imread(image_to_read);
    
    A = segmentationMask;
    tracks_in_fnumber = trackindex(ncells_cumsum(frame_number)+1:ncells_cumsum(frame_number+1));
    
    % change for specific selection - track index
    selection = [39,40];
    st = specific_tracks(selection,tracks_in_fnumber);
    stats(frame_number,:) = [nnz(st) ncells(frame_number)];
    
    B = changem(A,st,1:ncells(frame_number)); %new Mask
    label_frame(A,B,tracks_in_fnumber,greyscale,frame_number,cmap,tracked);        
    
    F(frame_number) = getframe(gcf);
    disp(frame_number);
end
%%
% play the recorded movie
implay(F);
%%
% save
save G_m1 F stats counts_idx cmap selection tracked;
%%
% command to load a saved movie
%load G_m1;
%%
% helpful commands to find a selection of tracks
%counts = longest_tracks(trackindex);
%counts_idx = [counts (1:size(counts)).'];
%%
% histogram plot
%figure();
%hist(counts_idx(:,1));