%% Define parameters for tracker
% input = dedicated cellx output
% output = cellx output plus tracking and the individual penalties
% associated with the tracker
% (struct (from importsegm.m), f_area, f_disp, f_orientation, egrowth, penalty, drift, maxjump)
% s = output from importsegmGregor; rsp concantenated CellX output
% f_area = factor for area
% f_disp = displacement penalty; it is calculated on the predicted position
% and not the real one
% f_orientation = penalty for major axis rotation; for cerevisiae the
% penalty is useless therefore 0 - paramater might be important for pombe
% where the majoraxis is learly defined
% egrowth = penalty for cell growth. Cells almost never lose but most often
% gain volume. This factor ensures that this is penalyezed accordingly:
% larger penalty for area loss than area gain.
% penalty = max amount the sum of the different penalties can reached. If
% larger, the its automatically set to this value. Currentltly set at 2000
% drift = drift of current tracked. The cell is tracked an from the
% previous movement, the new position is calculated. 1 would be the exact
% same movement as before. Currently set at 0.7 - 0.8
% maxjump = number of frames where a cell is not present. The idea is to
% correct missegmentation. Each skipped frame gets an additional penalty (set in addtrackindex, hardcoded).

% (s, f_area, f_disp, f_orientation, eg, penalty, drift, maxjump)
tracked = addtrackindex (s, 0.5e4, 2, 0, 0.03, 2e3, 0.7, 2);

% Save results 
outDir = 'F:\Data\Projects\fabian\TrackingLineage_from_Fabian\results_matlab';
strg = sprintf('%%.%dd', 5);
for i = 1 : numel(unique(tracked.cellframe))

    indices = find(tracked.cellframe == i);

    M = [ ...
        tracked.trackindex(indices), ...
        tracked.cost(indices), ...
        tracked.cost1(indices), ...
        tracked.cost2(indices), ...
        tracked.cost3(indices), ...
        tracked.cost4(indices)];

    fName = fullfile(outDir, ['cells_', sprintf(strg, i), '.txt']);
    dlmwrite(fName, M, 'delimiter', '\t', 'precision', 8);

    %i
end


