%-------------------------------------------------------------------------%
% Testing optical flow to estimate vectors to estimate movement on pads.
% Based on MathWorks example.
% Andreas Cuny 20160831
%-------------------------------------------------------------------------%

% Get path and filenames and save into struct
settings.ImgFilePath = uigetdir('Select the image directory');
ImageFiles = dir(fullfile(settings.ImgFilePath, '*.tif'));
settings.ImageFilesNames = {ImageFiles.name};

optical = vision.OpticalFlow( ... 
     'OutputValue', 'Horizontal and vertical components in complex form', 'Method', 'Lucas-Kanade');
shapes = vision.ShapeInserter
shapes.Shape = 'Lines';
shapes.BorderColor = 'white';
hVideoIn = vision.VideoPlayer;
hVideoIn.Name = 'Original Video';
hVideoOut = vision.VideoPlayer;
hVideoOut.Name = 'Motion Detected Video';
 
N = numel(settings.ImageFilesNames);
nFrames = 1;
while nFrames < 2 % loop over all frames0 
    Data = im2double(imread(fullfile(settings.ImgFilePath, settings.ImageFilesNames{nFrames})));
    Data = imresize( Data, 1/1);
    r = 1:length(Data(:,1));
    c = 1:length(Data(1,:));
    [X, Y] = meshgrid(c,r);
    
    optFlow = step(optical, Data);
   
    optFlow_DS = optFlow(r, c);
    H = imag(optFlow_DS)*50;
    V = real(optFlow_DS)*50;
    
    lines = [Y(:)'; X(:)'; Y(:)'+V(:)';X(:)'+H(:)'];
    DataOut = step(shapes, Data', lines');
    
    step(hVideoIn, Data')
    step(hVideoOut,DataOut)
    
    nFrames = nFrames +1;
    
end

%% Way to extract line positions. Overlay them with image. Get coordinates for cell track 1 in Frame 1 ...
% and find shape coordinates with pdist and plot result
point1 = [200, 200, 600, 600];
bild = insertShape(Data', 'Line', lines(:,:)', 'Color', 'red');
imshow(bild)
figure
bild2 = insertShape(Data', 'Line', point1, 'Color', 'red');
imshow(bild2)