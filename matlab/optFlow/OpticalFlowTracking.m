function varargout = OpticalFlowTracking(varargin)
% OPTICALFLOWTRACKING MATLAB code for OpticalFlowTracking.fig
%      OPTICALFLOWTRACKING, by itself, creates a new OPTICALFLOWTRACKING or raises the existing
%      singleton*.
%
%      H = OPTICALFLOWTRACKING returns the handle to a new OPTICALFLOWTRACKING or the handle to
%      the existing singleton*.
%
%      OPTICALFLOWTRACKING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPTICALFLOWTRACKING.M with the given input arguments.
%
%      OPTICALFLOWTRACKING('Property','Value',...) creates a new OPTICALFLOWTRACKING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OpticalFlowTracking_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OpticalFlowTracking_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OpticalFlowTracking

% Last Modified by GUIDE v2.5 06-Sep-2016 16:30:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OpticalFlowTracking_OpeningFcn, ...
                   'gui_OutputFcn',  @OpticalFlowTracking_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OpticalFlowTracking is made visible.
function OpticalFlowTracking_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OpticalFlowTracking (see VARARGIN)

% Choose default command line output for OpticalFlowTracking
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OpticalFlowTracking wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OpticalFlowTracking_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in LoadSettingsBtn.
function LoadSettingsBtn_Callback(hObject, eventdata, handles)
% hObject    handle to LoadSettingsBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load('OpticalFlowTracking.mat');
load_listbox(settings.CellXFiles, handles);

function load_listbox(dir_path, handles)
[sorted_names,sorted_index] = sortrows({dir_path.name}');
handles.file_names = sorted_names;
handles.is_dir = [dir_path.isdir];
handles.sorted_index = sorted_index;
guidata(handles.figure1,handles)
set(handles.listbox1,'String',handles.file_names, 'Value',1)

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings;

% Get index of selected item
IndexSelected = get(hObject,'Value');
handles.IndexSelected = IndexSelected;
% Load image end display it on specified axes
SelectedImg = imread(fullfile(settings.ImgFilePath, settings.ImageFilesNames{IndexSelected}));
handles.SelectedImg = SelectedImg;
% Check if CellX data is tracked
if isfield(settings, 'CellXTxtDataTracked') == 0
    msgbox('Please Track Data first, or load RelationSelector settings from previous run!')
else
    % Handle Tracked data
    %-------------------------------------------------------------------------------------%
    % Add image annotations for cell trackindex neighbours
    axes(handles.axes1)
    set(handles.axes1, 'XScale', 'linear');
    set(handles.axes1, 'box', 'on');
    imshow(SelectedImg)
    CellXTxtDataTracked = settings.CellXTxtDataTracked;
    set(handles.GetTrackIdxCoordsPop, 'string', num2str(sort(CellXTxtDataTracked(IndexSelected).trackindex)));
    CellCentersTxt = zeros(numel(CellXTxtDataTracked(IndexSelected).trackindex),2);
    CellTracknumbersTxt = zeros(numel(CellXTxtDataTracked(IndexSelected).trackindex),3);
    for i = 1:numel(CellXTxtDataTracked(IndexSelected).trackindex)
       CellCentersTxt(i, 1) = CellXTxtDataTracked(IndexSelected).cell_center_x(i);
       CellCentersTxt(i, 2) = CellXTxtDataTracked(IndexSelected).cell_center_y(i);
       CellTracknumbersTxt(i,1) = CellXTxtDataTracked(IndexSelected).cell_center_x(i)+5;
       CellTracknumbersTxt(i,2) = CellXTxtDataTracked(IndexSelected).cell_center_y(i);
       CellTracknumbersTxt(i,3) = CellXTxtDataTracked(IndexSelected).trackindex(i);
    end
     handles.ImgCellCentersTxt = text( CellCentersTxt(:, 1) ,  CellCentersTxt(:, 2) , 'x', 'Color', 'red', 'FontSize', 15);
     handles.ImgTrackindexTxt = text(CellTracknumbersTxt(:,1), CellTracknumbersTxt(:,2), num2str(CellTracknumbersTxt(:,3)), 'Color', 'green', 'FontSize', 15);
end

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SelImgFolderBtn.
function SelImgFolderBtn_Callback(hObject, eventdata, handles)
% hObject    handle to SelImgFolderBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
settings.ImgFilePath = uigetdir('Select the Image Directory of the experiment');
handles.ImgFilePath = settings.ImgFilePath;
ImageFiles = dir(fullfile(settings.ImgFilePath, '*.tif'));
settings.ImageFilesNames = {ImageFiles.name};
handles.ImageFilesNames = settings.ImageFilesNames;
assignin('base', 'ImgFilePath', handles.ImgFilePath);
assignin('base', 'ImageFilesNames', handles.ImageFilesNames);


settings.CellsFilePath = uigetdir('Select the CellX Segmentation Result Directory of the experiment');
handles.CellsFilePath = settings.CellsFilePath;
assignin('base', 'CellsFilePath', settings.CellsFilePath);
CellXFiles = dir(fullfile(settings.CellsFilePath, 'cells*.txt'));
settings.CellXFiles = CellXFiles;
settings.CellXFilesNames = {CellXFiles.name};
handles.CellXFilesNames = settings.CellXFilesNames;
assignin('base', 'CellXFileNames', settings.CellXFilesNames);
load_listbox(CellXFiles, handles);

% Load CellX data for selected frame
CellXTxtData = get_CellXTxtData( handles.CellsFilePath, handles.CellXFilesNames, 35);
handles.CellXTxtData = CellXTxtData;
settings.CellXTxtData = CellXTxtData;
assignin('base', 'CellXTxtData', CellXTxtData);

save('OpticalFlowTracking.mat', 'settings')

% --- Executes on button press in TrackCellsBtn.
function TrackCellsBtn_Callback(hObject, eventdata, handles)
% hObject    handle to TrackCellsBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings;
prompt={'Enter the factor for area [f_{area}]:',...
        'Enter the displacement penalty [f_{disp}]; (it is calculated on the predicted position and not the real one):', ...
        'Enter the penalty for major axis rotation [f_{orientation}]; (for cerevisiae the penalty is useless therefore 0 - paramater might be important for pombe where the majoraxis is learly defined) :',...
        'Enter the penalty for cell growth [e_{growth}]:', ...
        'Enter the  max amount the sum of the different penalties can reached [penalty]:', ...
        'Enter the drift of current tracked [drift]: ', ...
         'Enter the number of frames where a cell is not present [maxjump]:'};
name='Input arguments for addtrackindex function.';
numlines=1;
defaultanswer={'0.5e4', '2', '0', '0.03', '2e3', '0.7', '4'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
UserInput=inputdlg(prompt,name,numlines,...
                defaultanswer,options);
assignin('base', 'UserInput', UserInput)
CellXTxtDataTracked = addtrackindex_cunya(settings.CellXTxtData, ...
    str2double(cell2mat(UserInput(1))), str2double(cell2mat(UserInput(2))), ...
    str2double(cell2mat(UserInput(3))), str2double(cell2mat(UserInput(4))), ...
    str2double(cell2mat(UserInput(5))), str2double(cell2mat(UserInput(6))), ...
    str2double(cell2mat(UserInput(7))));
assignin('base', 'CellXTxtDataTracked', CellXTxtDataTracked)
settings.CellXTxtDataTracked = CellXTxtDataTracked;
save('OpticalFlowTracking.mat', 'settings');


% --- Executes on selection change in GetTrackIdxCoordsPop.
function GetTrackIdxCoordsPop_Callback(hObject, eventdata, handles)
%-------------------------------------------------------------------------%
% * [ ] Test of optical flow using Lucas-Kanade algo.
%-------------------------------------------------------------------------%
global settings
CellIDPopSelection = get(handles.GetTrackIdxCoordsPop, 'Value');
CellTrack = str2num(get(handles.GetTrackIdxCoordsPop, 'String'));
CellTrack = CellTrack(CellIDPopSelection);
assignin('base', 'CellTrack', CellTrack);
assignin('base', 'CellIDPopSelection', CellIDPopSelection);
FrameNumber = handles.IndexSelected;

optical = vision.OpticalFlow( ... 
     'OutputValue', 'Horizontal and vertical components in complex form', 'Method', 'Lucas-Kanade');
    Data = im2double(imread(fullfile(settings.ImgFilePath, settings.ImageFilesNames{FrameNumber})));
    % Subset data to a small ROI with cells to find out how algo works and
    % how it could be used to find a movement direction (vector) as well as
    % a possible moving distance ( vector length).
    SubData = Data(200:400,1000:1200);
    Data = imresize( Data, 1/1);
    r = 1:length(SubData(:,1));
    c = 1:length(SubData(1,:));
    [X, Y] = meshgrid(c,r);
    
    optFlow = step(optical, SubData);
   
    optFlow_DS = optFlow(r, c);
    H = imag(optFlow_DS)*50;
    V = real(optFlow_DS)*50;
    
    OptLines = [Y(:)'; X(:)'; Y(:)'+V(:)';X(:)'+H(:)'];

% Get CellTrack image coordinates
%-------------------------------------------------------------------------%
% * [ ] Implement selection of opt flow 'lines' vectors for region around
% cell center. ATM having memory issue.
centerMx= settings.CellXTxtDataTracked(FrameNumber).cell_center_x(settings.CellXTxtDataTracked(FrameNumber).trackindex == CellTrack); 
centerMy= settings.CellXTxtDataTracked(FrameNumber).cell_center_y(settings.CellXTxtDataTracked(FrameNumber).trackindex == CellTrack);
sprintf('Coordinates are %d and %d', centerMx, centerMy)
set(handles.CoordsTxt, 'String', sprintf('Cell coordinates are x:%d and y:%d', centerMx, centerMy))

CenterCoords = find(OptLines == centerMx & centerMy);

assignin('base', 'CenterCoords', CenterCoords)
assignin('base', 'OptLines', OptLines)

bild2 = insertShape(SubData', 'Line', OptLines(:,14000:15000)', 'Color', 'red');
axes(handles.axes2)
imshow(bild2)
%-------------------------------------------------------------------------%
% * [ ] Try a way to find index of coordinates around a certain distance. 
%       Maybe convert OptLines to struct with fields start_x, start_y,
%       end_x and end_y, then find all lines in a euclidean dist of say 40px 
%       around start_x_y
%-------------------------------------------------------------------------%
% Not working due to out of memory problem. Find a way to cut it down to a
% smaller area. Maybe with find around a search radius?
%-------------------------------------------------------------------------%
% LineData = struct('start_x', OptLines(1,:)', 'start_y', OptLines(2,:)', 'end_x', OptLines(3,:)', 'end_y', OptLines(4,:)');
% Coords = [LineData.start_x, LineData.start_y];
% CellDist = pdist(Coords) <= 20;
% blab.cell_distances = squareform(CellDist);
% Coords(Coords(:,1) == centerMx & Coords(:,2) == centerMy)
%-------------------------------------------------------------------------%

% --- Executes during object creation, after setting all properties.
function GetTrackIdxCoordsPop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to GetTrackIdxCoordsPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
