Description:
	The idea of using optical flow in our 2D cell tracker is to improve its function in cases of bad segmentation or unusual movements of the cells or whole cell colonies on pads (FOV).
	
Idea: 
	* [ ] Get optFlow vectors for every frame. Calc for each cell (or maybe only region with multiple cells) a main flow vector (mean of all found vectors) to estimate the direction/angle which then can be used as penalty in the tracker.
	* [ ] Find if there is also a relation between the vector length and the real movement on the pad. If that is possible one could predict where the new cell center should be and therefore check if there is a cell_index closeby. (one could also improve segmentation after tracking with resegmenting the movie thelling CellX where to place a new seed with slightly adapted parameters).

Current Test implementation:
	Matlab GUI ('OpticalFlowTracking.m') loads images and CellX results. Tracks results. And calculates optFlow with Lucas-Kanade algo. 
	Currently it displays only a subselection of the whole frame.
	Current task find/select only those vectors belonging to one cell (set of coordinates xy) and a certain euclidean distance around it.