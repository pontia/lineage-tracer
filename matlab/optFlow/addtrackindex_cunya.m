function s = addtrackindex (s, f_area, f_disp, f_orientation, eg, penalty, drift, maxjump)
%%
% tracking function; reimplementation of Markus tracker
% nothing really changed; except that we return the matrix of the
% calculated cost for further aalysis
% initialization first frame
start= 1;

% Trackindex and cost initialization
for r = 1:numel(s)
s(r).trackindex= NaN(length(s(r).cell_frame),1);
s(r).cost= NaN(length(s(r).cell_frame),1);
s(r).cost1= NaN(length(s(r).cell_frame),1);
s(r).cost2= NaN(length(s(r).cell_frame),1);
s(r).cost3= NaN(length(s(r).cell_frame),1);
s(r).cost4= NaN(length(s(r).cell_frame),1);
end
ofl= length(s(1).cell_frame);
s(1).trackindex(1:ofl) = 1:ofl;
% cost initialization
s(1).cost(1:ofl) = 0;
s(1).cost1(1:ofl) = 0;
s(1).cost2(1:ofl) = 0;
s(1).cost3(1:ofl) = 0;
s(1).cost4(1:ofl) = 0;

oldframe= struct('age',ones(ofl,1),'trackindex',(1:ofl).','dif_x',zeros(ofl,1),'dif_y',zeros(ofl,1),...
    'cellcenterx',s(1).cell_center_x,'cellcentery',s(1).cell_center_y,'cellarea',...
    s(1).cell_area,'cellorientation',s(1).cell_orientation);
%%
% here, the tracking starts
numberoftracks= ofl;

%
Frames = [];
for j = 1: length(s)
      Frames(j) = unique(s(j).cell_frame);
end
    %
u= Frames; %length(s(1).cell_frame);
u(1)=[];
assignin('base', 'u', u)
lu = length(u);
kk= 1;
while kk<= lu % lu change if only specific frames needed
    framenumber= u(kk);
    %%

    
    tic
    %new frame
    newframe= struct('cellcenterx',s(framenumber).cell_center_x,'cellcentery',s(framenumber).cell_center_y,...
        'cellarea',s(framenumber).cell_area,'cellorientation',s(framenumber).cell_orientation);
    
    %A costMat based on newframe oldframe and weight parameters
    [A, A1, A2, A3, A4]= weights_age(newframe, oldframe, f_area, f_disp, f_orientation, eg, penalty, drift, kk);
    assignment= lapjv(A);
    
    toc
    
    %%
    tic
    l = length(newframe.cellarea);
    x = ismember(assignment,1:l);
    x_x = find(x==1);
    i=1;
    
    B= A;
    cost= NaN(l,1);
    while i <= numel(oldframe.cellarea)
        j= assignment(i);
        cost(j)= A(i,j);
        B(i,j)= inf;
        i=i+1;
    end
    % assignment2= lapjv(B); if intrested in second best assignment
    
    i=1;   
    cost1= NaN(l,1);
    while i <= numel(oldframe.cellarea)
        j= assignment(i);
        cost1(j)= A1(i,j);
        i=i+1;
    end
    i=1; 
    cost2= NaN(l,1);
    while i <= numel(oldframe.cellarea)
        j= assignment(i);
        cost2(j)= A2(i,j);
        i=i+1;
    end
    i=1;   
    cost3= NaN(l,1);
    while i <= numel(oldframe.cellarea)
        j= assignment(i);
        cost3(j)= A3(i,j);
        i=i+1;
    end    
    i=1;
    cost4= NaN(l,1);
    while i <= numel(oldframe.cellarea)
        j= assignment(i);
        cost4(j)= A4(i,j);
        i=i+1;
    end
    %keep old tracks which could not be assigned (not segmented or cost was too high)
    veryold = oldframe;
    SNames = fieldnames(oldframe);
    loop1=1;
    while loop1 <= numel(SNames)
        loop2= numel(x_x);
        while loop2> 0
            veryold.(SNames{loop1})(x_x(loop2))= [];
            loop2 = loop2-1;
        end
        loop1 = loop1+1;
    end
    
    %remove too old old tracks
    too_old = (find(veryold.age > maxjump));
    loop1 = 1;
    while loop1 <= numel(SNames)
        loop2= numel(too_old);
        while loop2> 0
            veryold.(SNames{loop1})(too_old(loop2))= [];
            loop2 = loop2-1;
        end
        loop1 = loop1+1;
    end
    
    % displacement stored but not used atm
    y_y = assignment(x_x);
    dif_x = zeros(l,1);
    dif_y = zeros(l,1);
    dif_x(y_y) = (newframe.cellcenterx(y_y) - oldframe.cellcenterx(x_x))./oldframe.age(x_x);
    dif_y(y_y) = (newframe.cellcentery(y_y) - oldframe.cellcentery(x_x))./oldframe.age(x_x);
    
    veryold.age= veryold.age+1;
    
    new_trackindex = zeros(l,1);
    new_trackindex(y_y) = oldframe.trackindex(x_x);
    
    where = find(new_trackindex==0);
    add = length(where);
    new_trackindex(where)= numberoftracks+1:numberoftracks+add;
    
    % asign new trackindex
    s(framenumber).trackindex = new_trackindex;
    numberoftracks = numberoftracks + add;
    
    % asign costs
    s(framenumber).cost = cost(1:numel(new_trackindex));
    s(framenumber).cost1 = cost1(1:numel(new_trackindex));
    s(framenumber).cost2 = cost2(1:numel(new_trackindex));
    s(framenumber).cost3 = cost3(1:numel(new_trackindex));
    s(framenumber).cost4 = cost4(1:numel(new_trackindex));
    
    oldframe.age = [ones(l,1); veryold.age];
    oldframe.trackindex= [new_trackindex; veryold.trackindex];
    oldframe.dif_x= [dif_x; veryold.dif_x];
    oldframe.dif_y= [dif_y; veryold.dif_y];
    oldframe.cellcenterx= [newframe.cellcenterx; veryold.cellcenterx];
    oldframe.cellcentery= [newframe.cellcentery; veryold.cellcentery];
    oldframe.cellarea= [newframe.cellarea; veryold.cellarea];
    oldframe.cellorientation= [newframe.cellorientation; veryold.cellorientation];
    kk = kk +1;
    toc
    
    disp(kk);
    
end
end
