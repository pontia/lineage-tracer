function [ CellXTxtDataOut ] = get_CellXTxtData( PathNameTracking, FileNameTracking, NrowCellxTxt )
%get_CellXTxtData Loads the CellX segmenation results stored in
% text files (cells0000x.txt) into a cell struct.
%   PathNameTracking:      Path to the file          
%   FileNameTracking:      File name
%   NrowCellxTxt:          Nr of heading columns (normally 35)
%
% Usage: get_CellXTxtData( PathNameTracking, FileNameTracking, NrowCellxTxt )
%
% Output:                  Struct containing CellX Result data
%
% 20160817, Andreas Cuny

% Preallocate for speed
CellXTxtData = [];
FieldNames = {};

% Get column names
fileID = fopen(fullfile(PathNameTracking, FileNameTracking{1}));
FieldNamesTemp = textscan(fileID,'%s', NrowCellxTxt, 'delimiter','\t');
fclose(fileID);

% Loop over all image frames (files)
for k = 1:NrowCellxTxt
FieldNames{k} = strrep(FieldNamesTemp{1,1}{k,1}, '.', '_');
end
assignin('base', 'FiledNames', FieldNames);

for n = 1:numel(FileNameTracking)

% Working but not elegant!
fileID = fopen(fullfile(PathNameTracking, FileNameTracking{n}));
CellXTxtDataTemp = textscan(fileID,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ','delimiter','\t', 'HeaderLines' ,1);
fclose(fileID);

% Concentrate data in struct
for m =1:NrowCellxTxt
value = {CellXTxtDataTemp{1,m}};
CellXTxtData(n).(FieldNames{m})=value{1};
end
end

% Find a way how to conzentrate cell array into one field instead of having
% cells for each frames. But Attention if that would be changed.
% addtrackindex_cunya.m needs to be changed accordingly!!!
for m =1:NrowCellxTxt
CellXTxtData(1).(FieldNames{m}) = CellXTxtData.(FieldNames{m});
end
assignin('base', 'CellXTxtDatagtest', CellXTxtData)

CellXTxtDataOut = CellXTxtData;




end

