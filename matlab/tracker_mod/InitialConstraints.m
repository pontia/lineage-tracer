function varargout = InitialConstraints(varargin)
% INITIALCONSTRAINTS MATLAB code for InitialConstraints.fig
%      INITIALCONSTRAINTS, by itself, creates a new INITIALCONSTRAINTS or raises the existing
%      singleton*.
%
%      H = INITIALCONSTRAINTS returns the handle to a new INITIALCONSTRAINTS or the handle to
%      the existing singleton*.
%
%      INITIALCONSTRAINTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INITIALCONSTRAINTS.M with the given input arguments.
%
%      INITIALCONSTRAINTS('Property','Value',...) creates a new INITIALCONSTRAINTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before InitialConstraints_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to InitialConstraints_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help InitialConstraints

% Last Modified by GUIDE v2.5 22-Dec-2016 10:52:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @InitialConstraints_OpeningFcn, ...
                   'gui_OutputFcn',  @InitialConstraints_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before InitialConstraints is made visible.
function InitialConstraints_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to InitialConstraints (see VARARGIN)

linkaxes([handles.axes1, handles.axes2])
global settings
%load('trackersettingsold.mat')
%load('constraint_settings.mat')
%settings = settings_old;
%settings = constraint_settings;
maxFrame = numel(settings.CellXTxtData);
assignin('base', 'settings', settings)
handles.maxFrame = maxFrame;
Dfieldnames = fieldnames(settings.CellXTxtData);
set(handles.threshcatPop, 'String', Dfieldnames)
%load_listbox(settings.CellXFiles, handles);

% Read an image to get image dimensions:
settings.ImgSize = size(imread(fullfile(settings.ImgFilePath, settings.ImageFilesNames{1})));
settings.xcur = [0, settings.ImgSize(1)];
settings.ycur = [0, settings.ImgSize(1)];
set(handles.axes1, 'XLim', settings.xcur)
set(handles.axes1, 'YLim', settings.ycur)

handles.pt = 0;
handles.pt = 1;
handles.MaxYInt = 1;
handles.n = handles.MaxYInt;

% ImgCont = get_ControlImgs(settings.CellsFilePath, 1, handles.axes2);
% axes(handles.axes2)
% 
% axes(handles.axes1)
% set(handles.axes1, 'XScale', 'linear');
% set(handles.axes1, 'box', 'on');
% % Load image end display it on specified axes
% SelectedImg = imread(fullfile(settings.ImgFilePath, settings.ImageFilesNames{1})); %410+
% ImgCurr = imshow(SelectedImg);
% 
% settings.ImgCurr = ImgCurr;
load_images(settings, handles)

hold(handles.axes3)
handles.h_r_line = line([handles.pt handles.pt], [0 handles.MaxYInt],...
    'LineWidth', 4, 'Color', 'r', 'ButtonDownFcn', @(hObject,eventdata)InitialConstraints('startDragR_Fcn',hObject,eventdata,guidata(hObject)));


% Choose default command line output for InitialConstraints
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes InitialConstraints wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = InitialConstraints_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in maskBtn.
function maskBtn_Callback(hObject, eventdata, handles)
% hObject    handle to maskBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
axes(handles.axes2)
handles.poly = impoly(handles.axes1);
drawnow
Mask = createMask(handles.poly);
[Midx] = find(Mask == 1);
assignin('base', 'Mask', Mask)
[Mrr,Mcc] = ind2sub(settings.ImgSize(1), Midx); % Note could also be the ImgSize(2)!!!

%vertices = getposition(handles.poly);

%CellsToDelete = zeros(numel(settings.CellXTxtData, 1));
for l = 1:numel(settings.CellXTxtData)
% Find coordinates of cells outside mask area. Later Delete all cells outside mask
% area.
OutsideMaskIdx = ~inpolygon(settings.CellXTxtData(l).cell_center_x, settings.CellXTxtData(l).cell_center_y, Mcc, Mrr);
%if l == 1
    CellsToDelete{l} = settings.CellXTxtData(l).cell_index(OutsideMaskIdx);
    InsideMaskIdx{l} = ~OutsideMaskIdx;
    sprintf('Status: Calculating mask for frame %d', l)
%else
  %  CellsToDelete{l} = vertcat(CellsToDelete, settings.CellXTxtData(l).cell_index(OutsideMaskIdx));
%end
end
settings.CellsToDelete = CellsToDelete;
% Find coordinates of cells within mask area. Delete all cells outside mask
% area.
% Crow = ismember(settings.CellXTxtData(handles.CurrentFrame).cell_center_y, Mrr);
% Ccol= ismember(settings.CellXTxtData(handles.CurrentFrame).cell_center_x, Mcc); 
% % Selects inverse matches == cells outside of mask. If true ready to delete
% % and exclude from tracker.
% Crowi = ~Crow; 
% Ccoli = ~Ccol;
% Sanity check    
CellCentersTxtMx(:, 1) = settings.CellXTxtData(handles.CurrentFrame).cell_center_x(~InsideMaskIdx{handles.CurrentFrame});
CellCentersTxtMy(:, 1) = settings.CellXTxtData(handles.CurrentFrame).cell_center_y(~InsideMaskIdx{handles.CurrentFrame});
CellCentersTxtMkx(:, 1) = settings.CellXTxtData(handles.CurrentFrame).cell_center_x(InsideMaskIdx{handles.CurrentFrame});
CellCentersTxtMky(:, 1) = settings.CellXTxtData(handles.CurrentFrame).cell_center_y(InsideMaskIdx{handles.CurrentFrame});
% Plot 
axes(handles.axes1)
handles.CellCentersTxtMk = text( CellCentersTxtMkx(:, 1) ,  CellCentersTxtMky(:, 1) , 'x', 'Color', 'green', 'FontSize', 15);
hold on
handles.CellCentersTxtM = text( CellCentersTxtMx(:, 1) ,  CellCentersTxtMy(:, 1) , 'x', 'Color', 'red', 'FontSize', 15);
hold off
set(handles.CellCentersTxtMk,'Clipping','on')
set(handles.CellCentersTxtM,'Clipping','on')

set(handles.statusTxt, 'String', 'Status: Mask created.')

%Update handles structure
guidata(hObject, handles);

% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global settings
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
end

maxFrame = handles.maxFrame;
CurrentFrame = get(hObject,'Value');
if CurrentFrame == 0
    CurrentFrame = 1;
end
handles.CurrentFrame = round(CurrentFrame * maxFrame);

load_histogram(settings, handles)
load_images(settings, handles)

set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
%Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
handles.CurrentFrame = 1;
% set(handles.slider1, 'min', 0);
% set(handles.slider1, 'max', 1);
% set(handles.slider1, 'Value', 0);
 %Update handles structure
 guidata(hObject, handles);



% --- Executes on selection change in threshcatPop.
function threshcatPop_Callback(hObject, eventdata, handles)
% hObject    handle to threshcatPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns threshcatPop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from threshcatPop
global settings
contents = cellstr(get(hObject,'String'));
handles.sel = contents{get(hObject,'Value')};
settings.threshcatPol_sel = handles.sel;
[n, bin] = hist(settings.CellXTxtData(handles.CurrentFrame).(handles.sel), 256);
disp(handles.sel)
axes(handles.axes3)
hist(settings.CellXTxtData(handles.CurrentFrame).(handles.sel), 256)
handles.MinFitInt = min(bin)-0.05;
handles.MaxFitInt = max(bin)-0.05;
handles.MaxYInt = max(n)+1;
% hold(handles.axes3)
% handles.h_b_line = line([handles.MinFitInt handles.MinFitInt], [0 handles.MaxYInt],...
%     'LineWidth', 4, 'Color', 'b', 'ButtonDownFcn', @(hObject,eventdata)InitialConstraints('startDragB_Fcn',hObject,eventdata,guidata(hObject)));
hold(handles.axes3)
handles.h_r_line = line([handles.MaxFitInt handles.MaxFitInt], [0 handles.MaxYInt],...
    'LineWidth', 4, 'Color', 'r', 'ButtonDownFcn', @(hObject,eventdata)InitialConstraints('startDragR_Fcn',hObject,eventdata,guidata(hObject)));
set(handles.figure1, 'WindowButtonUpFcn', @(hObject,eventdata)InitialConstraints('stopDragFcn',hObject,eventdata,guidata(hObject)));

set(handles.statusTxt, 'String', sprintf('Status: Selected threshold category %s.', handles.sel))
%Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function threshcatPop_CreateFcn(hObject, eventdata, ~)
% hObject    handle to threshcatPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% function startDragB_Fcn(hObject, eventdata, handles)
%         set(handles.figure1, 'WindowButtonMotionFcn', @(hObject,eventdata)InitialConstraints('draggingB_Fcn',hObject,eventdata,guidata(hObject))); 
% 
% function draggingB_Fcn(hObject, eventdata, handles)
%         pt = get(handles.axes3, 'CurrentPoint');
%         set(handles.h_b_line, 'XData', pt(1)*[1 1]);

function startDragR_Fcn(hObject, eventdata, handles)
        set(handles.figure1, 'WindowButtonMotionFcn', @(hObject,eventdata)InitialConstraints('draggingR_Fcn',hObject,eventdata,guidata(hObject)));
      
        
function draggingR_Fcn(hObject, eventdata, handles)
        global settings        
        pt = get(handles.axes3, 'CurrentPoint');
        handles.pt = pt;
        %handles.h_r_line = settings.h_r_line;
        set(handles.h_r_line, 'XData', pt(1)*[1 1]); 
        %settings.h_r_line =  pt(1)*[1 1];
        %Update handles structure
        guidata(hObject, handles);
       
function stopDragFcn(hObject, eventdata, handles)
        set(handles.figure1, 'WindowButtonMotionFcn', '');
    

% --- Executes on button press in thresholdBtn.
function thresholdBtn_Callback(hObject, eventdata, handles)
% hObject    handle to thresholdBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
else
    
 % Re enable new threshold 
 handles.h_r_line = line([handles.pt(1) handles.pt(1)], [0 handles.n+1],...
     'LineWidth', 4, 'Color', 'b', 'ButtonDownFcn', @(hObject,eventdata)InitialConstraints('startDragR_Fcn',hObject,eventdata,guidata(hObject)));
 set(handles.figure1, 'WindowButtonUpFcn', @(hObject,eventdata)InitialConstraints('stopDragFcn',hObject,eventdata,guidata(hObject)));
 
% Get user specified threshold
%if isfield('handles', 'h_r_line')
Threshold = get(handles.h_r_line, 'XData');
%else
%Threshold = settings.h_r_line;
%end
% Get user specified data column used to threshold data
sel = settings.threshcatPol_sel;
ConstraintDataTemp = settings.CellXTxtData(handles.CurrentFrame).(sel);
ConstraintData = ConstraintDataTemp > Threshold(1);
settings.ConstraintData = ConstraintData;
ValidData = ~ConstraintData; 

%CellCentersTxtC = NaN(numel(settings.CellXTxtData(1).cell_center_x),4);
%Update plot
CellCentersTxtC(:, 1) = settings.CellXTxtData(handles.CurrentFrame).cell_center_x(ConstraintData);
CellCentersTxtC(:, 2) = settings.CellXTxtData(handles.CurrentFrame).cell_center_y(ConstraintData);
CellCentersTxt(:, 1) = settings.CellXTxtData(handles.CurrentFrame).cell_center_x(ValidData);
CellCentersTxt(:, 2) = settings.CellXTxtData(handles.CurrentFrame).cell_center_y(ValidData);
axes(handles.axes1)
handles.h1 = text( CellCentersTxtC(:, 1) ,  CellCentersTxtC(:, 2) , 'x', 'Color', 'red', 'FontSize', 15);
hold on
handles.h2 = text( CellCentersTxt(:, 1) ,  CellCentersTxt(:, 2) , 'x', 'Color', 'green', 'FontSize', 15);
set(handles.h1,'Clipping','on')
set(handles.h2,'Clipping','on')
end
set(handles.statusTxt, 'String', 'Status: Finished data threshold visualization.')

%Update handles structure
guidata(hObject, handles);

% --- Executes on button press in appthresholdBtn.
function appthresholdBtn_Callback(hObject, eventdata, handles)
% hObject    handle to appthresholdBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
settings.CellXTxtDataRaw = settings.CellXTxtData;
NFrames = numel(settings.CellXTxtData);
Fnames = fieldnames(settings.CellXTxtData);
% Delete data below threshold
%-------------------------------------------------------------------------%
% Get user specified threshold
Threshold = get(handles.h_r_line, 'XData');
% Get user specified data column used to threshold data
sel = settings.threshcatPol_sel;
% Apply threshold for all frames
for m = 1:NFrames
    ConstraintDataTemp = settings.CellXTxtData(m).(sel);
    ConstraintData = ConstraintDataTemp < Threshold(1);
    % Apply threshold to all fieldnames
    for l = 1:numel(Fnames)
        settings.CellXTxtData(m).(Fnames{l})(ConstraintData) = [];
    end
end
save('ConstrainedData.mat', 'settings')
set(handles.statusTxt, 'String', 'Status: Saved thresholded data.')
%Update handles structure
guidata(hObject, handles);


% --- Executes on button press in clamaskBtn.
function clamaskBtn_Callback(hObject, eventdata, handles)
% hObject    handle to clamaskBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles,'poly')
    errordlg('No mask drawn')
else
    delete(handles.poly)
    delete(handles.CellCentersTxtMk)
    delete(handles.CellCentersTxtM)
end
set(handles.statusTxt, 'String', 'Status: Mask deleted')
%Update handles structure
guidata(hObject, handles);

% --------------------------------------------------------------------
function uitoggletool1_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
sprintf('zoom pushed2')
axes(handles.axes1)
settings.xcur = get(gca,'XLim');
settings.ycur = get(gca,'YLim');
assignin('base', 'settings', settings)
%Update handles structure
%guidata(hObject, handles);


% --- Executes on button press in toendrBtn.
function toendrBtn_Callback(hObject, eventdata, handles)
% hObject    handle to toendrBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings;
maxFrame = handles.maxFrame;
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
end

handles.CurrentFrame = maxFrame;
load_images(settings, handles);
load_histogram(settings, handles)
set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
set(handles.slider1, 'value', maxFrame/maxFrame)

%Update handles structure
guidata(hObject, handles);

% --- Executes on button press in toendlBtn.
function toendlBtn_Callback(hObject, eventdata, handles)
% hObject    handle to toendlBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings;
handles.CurrentFrame = 1;
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
end

load_images(settings, handles);
load_histogram(settings, handles)
set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
set(handles.slider1, 'value', 0)

%Update handles structure
guidata(hObject, handles);

% --- Executes on button press in plus10Btn.
function plus10Btn_Callback(hObject, eventdata, handles)
% hObject    handle to plus10Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
maxFrame = handles.maxFrame;
CurrentFrame = handles.CurrentFrame;
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
end

if CurrentFrame <= (maxFrame - 10)
    handles.CurrentFrame = CurrentFrame + 10;
    load_histogram(settings, handles);
     load_images(settings, handles);
    set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
    set(handles.slider1, 'value', handles.CurrentFrame/handles.maxFrame)
else
    handles.CurrentFrame = maxFrame;
    load_histogram(settings, handles);
    load_images(settings, handles);
    set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
    set(handles.slider1, 'value', handles.CurrentFrame/handles.maxFrame)
end
%Update handles structure
guidata(hObject, handles);

% --- Executes on button press in minus10Btn.
function minus10Btn_Callback(hObject, eventdata, handles)
% hObject    handle to minus10Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
CurrentFrame = handles.CurrentFrame;
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
end

if CurrentFrame >= (1 + 9)
    handles.CurrentFrame = CurrentFrame - 10;
    load_histogram(settings, handles);
    load_images(settings, handles);
    set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
    set(handles.slider1, 'value', handles.CurrentFrame/handles.maxFrame)
else
    handles.CurrentFrame = 1;
    load_histogram(settings, handles);
    load_images(settings, handles);
    set(handles.statusTxt, 'String', sprintf('Status: Image frame %d selected.', handles.CurrentFrame))
    set(handles.slider1, 'value', handles.CurrentFrame/handles.maxFrame)
end
%Update handles structure
guidata(hObject, handles);

function load_images(settings, handles)
% Delete threshold text marker if present
if isfield('handles', 'h1')
    delete(handles.h1)
    delete(handles.h2)
end
% Load control image and display it on the specified axes
ImgCont = get_ControlImgs(settings.CellsFilePath, handles.CurrentFrame, handles.axes2);
axes(handles.axes2)
settings.ImgCont = ImgCont;
% Load image and display it on the specified axes
axes(handles.axes1)
SelectedImg = imread(fullfile(settings.ImgFilePath, settings.ImageFilesNames{handles.CurrentFrame})); %+ 410
ImgCurr = imshow(SelectedImg);
settings.ImgCurr = ImgCurr;

 % Re enable new threshold 
 handles.h_r_line = line([handles.pt(1) handles.pt(1)], [0 max(handles.n)+1],...
     'LineWidth', 4, 'Color', 'g', 'ButtonDownFcn', @(hObject,eventdata)InitialConstraints('startDragR_Fcn',hObject,eventdata,guidata(hObject)));
 set(handles.figure1, 'WindowButtonUpFcn', @(hObject,eventdata)InitialConstraints('stopDragFcn',hObject,eventdata,guidata(hObject)));

guidata(handles.figure1,handles)
%set(handles.listbox1,'String',handles.file_names, 'Value',1)


function load_histogram(settings, handles)
if isfield(settings, 'threshcatPol_sel')
    [n, bin] = hist(settings.CellXTxtData(handles.CurrentFrame).(handles.sel), 256);
    handles.MaxYInt = max(n)+1;
    axes(handles.axes3)
    hist(settings.CellXTxtData(handles.CurrentFrame).(settings.threshcatPol_sel), 256)
    hold(handles.axes3)
    handles.h_r_line = line([handles.pt(1) handles.pt(1)], [0 max(n)+1],...
    'LineWidth', 4, 'Color', 'r','ButtonDownFcn', @(hObject,eventdata)InitialConstraints('startDragR_Fcn',hObject,eventdata,guidata(hObject)));
    set(handles.figure1, 'WindowButtonUpFcn', @(hObject,eventdata)InitialConstraints('stopDragFcn',hObject,eventdata,guidata(hObject)));
    guidata(handles.figure1,handles)
end
guidata(handles.figure1,handles)


% --- Executes on button press in ApplyMaskBtn.
function ApplyMaskBtn_Callback(hObject, eventdata, handles)
% hObject    handle to ApplyMaskBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings

 % Delete cells outside mask for each frame
for k = 1:numel(settings.CellXTxtData)
    settings.CellXTxtData = del_DataMeetsCriterionML( settings.CellXTxtData, k,  'cell_index' , settings.CellsToDelete{k});
end
set(handles.statusTxt, 'String', 'Status: Mask successfully applied on dataset.')


