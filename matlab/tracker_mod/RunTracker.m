function varargout = RunTracker(varargin)
% RUNTRACKER MATLAB code for RunTracker.fig
%      RUNTRACKER, by itself, creates a new RUNTRACKER or raises the existing
%      singleton*.
%
%      H = RUNTRACKER returns the handle to a new RUNTRACKER or the handle to
%      the existing singleton*.
%
%      RUNTRACKER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RUNTRACKER.M with the given input arguments.
%
%      RUNTRACKER('Property','Value',...) creates a new RUNTRACKER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RunTracker_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RunTracker_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RunTracker

% Last Modified by GUIDE v2.5 08-Dec-2016 10:21:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RunTracker_OpeningFcn, ...
                   'gui_OutputFcn',  @RunTracker_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RunTracker is made visible.
function RunTracker_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RunTracker (see VARARGIN)


set(handles.SaveProjBtn, 'Enable', 'off')
set(handles.RunTrackerBtn, 'Enable', 'off')
set(handles.ReconstructLinBtn, 'Enable', 'off')
% Choose default command line output for RunTracker
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RunTracker wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = RunTracker_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in CreateTrackerProjBtn.
function CreateTrackerProjBtn_Callback(hObject, eventdata, handles)
% hObject    handle to CreateTrackerProjBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
SetPaths(settings);
uiwait
if settings.isSetPathFigDeleted == 1
    uiresume
    set(handles.RunTrackerBtn, 'Enable', 'on')
    set(handles.SaveProjBtn, 'Enable', 'on')
    if settings.isProjectCreated == 1
        set(handles.StatusBarTxt, 'String', sprintf('Status: Project created successfully'))
    else
        set(handles.StatusBarTxt, 'String', sprintf('Status: No project created'))
    end
    
end
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in LoadProjBtn.
function LoadProjBtn_Callback(hObject, eventdata, handles)
% hObject    handle to LoadProjBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings;

[Load_filename, Load_pathname,] = uigetfile( ...
       {'*.mat','MAT-files (*.mat)';});
if isequal(Load_filename, 0)
    helpdlg('No file was selected. Nothing will be loaded.')
else
    load(fullfile(Load_pathname, Load_filename))
    if ~isequal(settings.CellXTxtData, 0)
        set(handles.SaveProjBtn, 'Enable', 'on')
        set(handles.RunTrackerBtn, 'Enable', 'on')
        set(handles.StatusBarTxt, 'String', sprintf('Status: Project loaded successfully'))
    else
        helpdlg('No valid project was created or loaded. Please try again.')
    end
    % Enable Lineage reconstruction when project was tracked before
    if isfield(settings, 'CellXTxtDataTracked')
        set(handles.ReconstructLinBtn, 'Enable', 'on')
    end
end
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in SaveProjBtn.
function SaveProjBtn_Callback(hObject, eventdata, handles)
% hObject    handle to SaveProjBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
% Save Project under Project name:
if isfield(settings, 'ProjectName')
save([settings.ProjectName, '.mat'], 'settings')
set(handles.StatusBarTxt, 'String', sprintf('Status: Project saved successfully'))
else
prompt = {'Enter a project name first'};
dlg_title = 'Save Project';
num_lines = 1;
defaultans = {''};
settings.ProjectName = inputdlg(prompt,dlg_title,num_lines,defaultans);
% handle no input to avoid error
save([settings.ProjectName, '.mat'], 'settings')
set(handles.StatusBarTxt, 'String', sprintf('Status: Project saved successfully'))
end
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in RunTrackerBtn.
function RunTrackerBtn_Callback(hObject, eventdata, handles)
% hObject    handle to RunTrackerBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
%-------------------------------------------------------------------------%
% Run tracker:
prompt={'Enter the factor for area [f_{area}]:',...
        'Enter the displacement penalty [f_{disp}]; (it is calculated on the predicted position and not the real one):', ...
        'Enter the penalty for major axis rotation [f_{orientation}]; (for cerevisiae the penalty is useless therefore 0 - paramater might be important for pombe where the majoraxis is learly defined) :',...
        'Enter the penalty for cell growth [e_{growth}]:', ...
        'Enter the  max amount the sum of the different penalties can reached [penalty]:', ...
        'Enter the drift of current tracked [drift]: ', ...
        'Enter the number of frames where a cell is not present [maxjump]: ', ...
        'Enter the radius [r] around each cell center that should be used for creating a unique cell barcode: ', ...
        'Enter the radius [rn] defining neighbouring cells around each cell center that should be used: ', ...
        'Enter the radius for filtering the vectors: '};
name='Input arguments for addtrackindex function.';
numlines=1;
defaultanswer={'0.5e4', '2', '0', '0.03', '2e3', '0.7', '4', '40', '40', '100'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
UserInput=inputdlg(prompt,name,numlines,...
                defaultanswer,options);
if ~isempty(UserInput)
    assignin('base', 'TrackerInput', settings.CellXTxtData)
    settings.CellXTxtDataTracked = addtrackindex_cunyaT(settings.CellXTxtData, ...
    settings.ImgFilePath, settings.ImageFilesNames, ...
    str2double(cell2mat(UserInput(1))), str2double(cell2mat(UserInput(2))), ...
    str2double(cell2mat(UserInput(3))), str2double(cell2mat(UserInput(4))), ...
    str2double(cell2mat(UserInput(5))), str2double(cell2mat(UserInput(6))), ...
    str2double(cell2mat(UserInput(7))), str2double(cell2mat(UserInput(8))), ...
    str2double(cell2mat(UserInput(9))), str2double(cell2mat(UserInput(10))));
    set(handles.ReconstructLinBtn, 'Enable', 'on')
    set(handles.StatusBarTxt, 'String', sprintf('Status: Tracking finished'))
else
    set(handles.StatusBarTxt, 'String', sprintf('Status: Nothing to track'))
end
%-------------------------------------------------------------------------%

% --- Executes on button press in ReconstructLinBtn.
function ReconstructLinBtn_Callback(hObject, eventdata, handles)
% hObject    handle to ReconstructLinBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Construct a questdlg with three options
global settings;
choice = questdlg('Select the cell division type that matches your data best', ...
	'Species division category', ...
	'Symmetrical Division','Asymmetrical Division', 'Asymmetrical Division');
% Handle response
switch choice
    case 'Symmetrical Division'
        settings.DivisionType = 'Symmetrical Division';
        disp([choice ' selected.'])
        PombeRelationViewer(settings)
    case 'Asymmetrical Division'
        settings.DivisionType = 'Asymmetrical Division';
        disp([choice ' selected.'])
        hAsymmetricalDivRelationViewer = AsymmetricalDivRelationViewer(settings);
        %AnalysisWdn(settings)
end
