function varargout = SetPaths(varargin)
% SETPATHS MATLAB code for SetPaths.fig
%      SETPATHS, by itself, creates a new SETPATHS or raises the existing
%      singleton*.
%
%      H = SETPATHS returns the handle to a new SETPATHS or the handle to
%      the existing singleton*.
%
%      SETPATHS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETPATHS.M with the given input arguments.
%
%      SETPATHS('Property','Value',...) creates a new SETPATHS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SetPaths_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SetPaths_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SetPaths

% Last Modified by GUIDE v2.5 17-Jan-2017 11:13:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SetPaths_OpeningFcn, ...
                   'gui_OutputFcn',  @SetPaths_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SetPaths is made visible.
function SetPaths_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SetPaths (see VARARGIN)
set(handles.LoadCellXDataBtn, 'Enable', 'off')
set(handles.ConstraintsBtn, 'Enable', 'off')
set(handles.OkBtn, 'Enable', 'off')
% Choose default command line output for SetPaths
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SetPaths wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SetPaths_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in SetImagePathBtn.
function SetImagePathBtn_Callback(hObject, eventdata, handles)
% hObject    handle to SetImagePathBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
% Path to images
ImgFilePath = uigetdir;
%ImgFilePath ='C:\Users\localadmin\Desktop\ImgDiv';
if ~isequal(ImgFilePath, 0) 
    ImageFiles = dir(fullfile(ImgFilePath, '*.tif'));
    ImageFilesNames = {ImageFiles.name}; %settings.ImageFilesNames;
    settings.ImageFilesNames = ImageFilesNames;
    settings.ImgFilePath = ImgFilePath;
    set(handles.ImagePathTxt, 'String', settings.ImgFilePath)
end
% Update handles structure
guidata(hObject, handles)


% --- Executes on button press in SetCellXTxtPathBtn.
function SetCellXTxtPathBtn_Callback(hObject, eventdata, handles)
% hObject    handle to SetCellXTxtPathBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
% Path to CellX Result files
if isfield(settings, 'ImgFilePath')
CellsFilePath = uigetdir(settings.ImgFilePath); % settings.CellsFilePath
else
CellsFilePath = uigetdir;
end
if ~isequal(CellsFilePath, 0)
    %CellsFilePath = 'C:\Users\localadmin\Desktop\ImgDiv\CellXResult\position010200';
    CellXFiles = dir(fullfile(CellsFilePath, 'cells*.txt'));
    CellXFilesNames = {CellXFiles.name}; %settings.CellXFilesNames;
    settings.CellXFilesNames = CellXFilesNames;
    settings.CellsFilePath = CellsFilePath;
    set(handles.CellsPathTxt, 'String', settings.CellsFilePath)
    % Path to CellX mat files
    CellXMatFiles = dir(fullfile(CellsFilePath, 'cells*.mat'));
    CellXMatFilesNames = {CellXMatFiles.name}; %settings.CellXFilesNames;
    settings.CellXMatFilesNames = CellXMatFilesNames;
    set(handles.MatPathTxt, 'String', settings.CellsFilePath)
end
% Update handles structure
guidata(hObject, handles)


% --- Executes on button press in SetCellXMatPathBtn.
function SetCellXMatPathBtn_Callback(hObject, eventdata, handles)
% hObject    handle to SetCellXMatPathBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
if isfield(settings, 'ImgFilePath')
CellsFilePathMat = uigetdir(settings.ImgFilePath); % settings.CellsFilePath
else
CellsFilePathMat = uigetdir;
end
if ~isequal(CellsFilePathMat, 0)
    settings.CellXMatFilesNames = CellsFilePathMat;
    % Path to CellX mat files
    CellXMatFiles = dir(fullfile(CellsFilePathMat, 'cells*.mat'));
    CellXMatFilesNames = {CellXMatFiles.name}; %settings.CellXFilesNames;
    settings.CellXMatFilesNames = CellXMatFilesNames;
    set(handles.MatPathTxt, 'String', CellsFilePathMat)
end
% Update handles structure
guidata(hObject, handles)



function CellXColumnsTxt_Callback(hObject, eventdata, handles)
% hObject    handle to CellXColumnsTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CellXColumnsTxt as text
%        str2double(get(hObject,'String')) returns contents of CellXColumnsTxt as a double
global settings
NcolsCellX = get(hObject,'String');
settings.NcolsCellX = str2double(NcolsCellX);
% Activate Load CellX Btn
set(handles.LoadCellXDataBtn, 'Enable', 'on')

% --- Executes during object creation, after setting all properties.
function CellXColumnsTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CellXColumnsTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConstraintsBtn.
function ConstraintsBtn_Callback(hObject, eventdata, handles)
% hObject    handle to ConstraintsBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
InitialConstraints(settings)

% --- Executes on button press in OkBtn.
function OkBtn_Callback(hObject, eventdata, handles)
% hObject    handle to OkBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
% Save Project under Project name:
if isfield(settings, 'ProjectName')
save([settings.ProjectName, '.mat'], 'settings')
settings.isProjectCreated = 1;
settings.isSetPathFigDeleted = 1;
else
helpdlg('Please give me a project name first!')
end
delete(gcf)

% --- Executes on button press in CancelBtn.
function CancelBtn_Callback(hObject, eventdata, handles)
% hObject    handle to CancelBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
settings.isProjectCreated = 0;
settings.isSetPathFigDeleted = 1;
delete(gcf)

% --- Executes on button press in LoadCellXDataBtn.
function LoadCellXDataBtn_Callback(hObject, eventdata, handles)
% hObject    handle to LoadCellXDataBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global settings
% Create Data struct
settings.CellXTxtData = get_CellXTxtData( settings.CellsFilePath, settings.CellXFilesNames, settings.NcolsCellX);
% Enable buttons for continuation
set(handles.ConstraintsBtn, 'Enable', 'on')
set(handles.OkBtn, 'Enable', 'on')


function ProjectNameTxt_Callback(hObject, eventdata, handles)
% hObject    handle to ProjectNameTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ProjectNameTxt as text
%        str2double(get(hObject,'String')) returns contents of ProjectNameTxt as a double
global settings
settings.ProjectName = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function ProjectNameTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ProjectNameTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ImagingIntervalInMinutesTxt_Callback(hObject, eventdata, handles)
% hObject    handle to ImagingIntervalInMinutesTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get the userinput and convert it to numeric
global settings
ImagingIntervalInMinutesStr = get(hObject,'String');
settings.ImagingIntervalInMinutes = str2double(ImagingIntervalInMinutesStr);

% --- Executes during object creation, after setting all properties.
function ImagingIntervalInMinutesTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImagingIntervalInMinutesTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
