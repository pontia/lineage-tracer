function data = addtrackindex_cunyaT(data, ImgFilePath, ImageFilesNames, f_area, f_disp, f_orientation, eg, penalty, drift, maxjump, r, rn, filter_support)
%%
% addtrackindex_cunyaT.m Re-implementation of Markus initial tracker
% with tracking verification based on quality control features.
% 
% Input:
%   data:                     Dataset [struct] With at least fields
%                             cell_index, cell_center_x and cell_center_y.
%   ImgFilePath:              Path [String] to directory of out-of-focus
%                             Brightfield images.
%   ImageFilesNames:          Brightfield images [Cell] filenames.
%   f_area:                   Factor for area [Int].
%   f_disp:                   Displacement penalty [Int]; (it is calculated 
%                             on the predicted position and not the real one)
%   f_orientation:            Penalty for major axis rotation [Float]; (for 
%                             cerevisiae the penalty is useless therefore 0 
%                             - paramater might be important for pombe where 
%                             the majoraxis is learly defined)
%   eg:                       Penalty for cell growth [Int]
%   penalty:                  Max amount the sum of the different penalties 
%                             can reached [Int]
%   drift:                    Allowed drift of current tracked [Float]: 
%   maxjump:                  Number of frames where a cell is not present [Int]
%   r:                        Radius [pixel] defining crop region around each 
%                             cell center for extracting the quality control
%                             feature (QC). 
%   rn:                       Radius [pixel] for finding neighbouring cells 
%                             for each cell center. Needed for tracker 
%                             verification. We look only at neighbouring 
%                             cells to verify tracks (higher sensitivity).
%                             feature (QC).
%   filter_support:           Radius [pixel] defining region around each 
%                             cell center where to find neighbouring cells.
% Output:                     
%   data:                     Tracked dataset [struct]
%
% 20161021, Andreas Cuny

start= 1;

% Trackindex and cost initialization
for fr = 1:numel(data)
data(fr).trackindex= NaN(length(data(fr).cell_frame),1);
data(fr).displacement_x= NaN(length(data(fr).cell_frame),1);
data(fr).displacement_y= NaN(length(data(fr).cell_frame),1);
data(fr).cost= NaN(length(data(fr).cell_frame),1);
data(fr).cost1= NaN(length(data(fr).cell_frame),1);
data(fr).cost2= NaN(length(data(fr).cell_frame),1);
data(fr).cost3= NaN(length(data(fr).cell_frame),1);
data(fr).cost4= NaN(length(data(fr).cell_frame),1);
%-------------------------------------------------------------------------%
% Quality Control (qc) field initialization as well as qc_dist
%-------------------------------------------------------------------------%
data(fr).qc = cell(length(data(fr).cell_frame),1); 
data(fr).qc_dist = cell(length(data(fr).cell_frame),1);
%-------------------------------------------------------------------------%
% Initialize new field name .cell_distances and fill with squaredmatrix
% where row equals trackindex and column its neighbours within rn
% (Neighbour radius)
%-------------------------------------------------------------------------%
data(fr).cell_distances = zeros(numel(data(fr).cell_index),numel(data(fr).cell_index));
Coords = [data(fr).cell_center_x, data(fr).cell_center_y];
CellDist = pdist(Coords) <= rn;
data(fr).cell_distances = squareform(CellDist);
%-------------------------------------------------------------------------%
% Field initialization for later data deletion
%-------------------------------------------------------------------------%
data(fr).qc_age= zeros(length(data(fr).cell_frame),1);
data(fr).cell_del= zeros(length(data(fr).cell_frame),1);
end
%-------------------------------------------------------------------------%
% Calculate qc features ( Note: could also be calculated before entering this
% addtrackindex_cunyaT function since it acctualy belongs to data initialisation. 
% Would save the ImgFilePath/Names arguments.
%-------------------------------------------------------------------------%
data = get_QualityControlT( data, ImgFilePath, ImageFilesNames, r) ;

ofl= length(data(1).cell_frame);
data(1).trackindex(1:ofl) = 1:ofl;
data(1).displacement_x(1:ofl) = 0;
data(1).displacement_y(1:ofl) = 0;
% Cost initialization
data(1).cost(1:ofl) = 0;
data(1).cost1(1:ofl) = 0;
data(1).cost2(1:ofl) = 0;
data(1).cost3(1:ofl) = 0;
data(1).cost4(1:ofl) = 0;

%-------------------------------------------------------------------------%
% Added fields 'filtered_dif_x', 'filtered_dif_y', 'cell_distances', 'qc',
% 'qc_dist', 'cell_del', 'qc_age' and their initialization.
%-------------------------------------------------------------------------%
oldframe= struct('age',ones(ofl,1),'trackindex',(1:ofl).',...
    'cell_center_x',data(1).cell_center_x,'cell_center_y',data(1).cell_center_y,...
    'dif_x',zeros(ofl,1),'dif_y',zeros(ofl,1),'filtered_dif_x',zeros(ofl,1),...
    'filtered_dif_y',zeros(ofl,1), 'cell_area',data(1).cell_area,'cell_orientation',data(1).cell_orientation,...
    'cell_distances',{data(1).cell_distances}, 'qc',{data(1).qc}, 'qc_dist',{data(1).qc_dist}, 'cell_del',zeros(ofl,1), 'qc_age',ones(ofl,1));

%%
% Here, the tracking starts
numberoftracks= ofl;
%u = unique(data.cell_frame);
%
Frames = [];
for j = 1: length(data)
      Frames(j) = unique(data(j).cell_frame);
end
    
u= Frames; %length(data(1).cell_frame);
u(1)=[];
assignin('base', 'u', u)
lu = length(u);
kk= 1;
while kk<= lu % lu change if only specific frames needed
    framenumber= u(kk);
    %%

    tic
    %-------------------------------------------------------------------------%
    % Added fields 'filtered_dif_x', 'filtered_dif_y', 'cell_distances', 'qc',
    % 'qc_dist', 'cell_del', 'qc_age' and their initialization.
    %-------------------------------------------------------------------------%
     %new frame
    newframe= struct('cell_center_x',data(framenumber).cell_center_x,'cell_center_y',data(framenumber).cell_center_y,...
        'cell_area',data(framenumber).cell_area,'cell_orientation',data(framenumber).cell_orientation, ...
        'trackindex',(1:ofl).', 'cell_distances',{data(framenumber).cell_distances}, ...
        'qc',{data(framenumber).qc}, 'qc_dist', {data(framenumber).qc_dist},...
        'cell_del', data(framenumber).cell_del,  'qc_age',ones(ofl,1));
    
    % A costMat based on newframe oldframe and weight parameters
    %-------------------------------------------------------------------------%
    % This function remained unchanged!
    %-------------------------------------------------------------------------%
    [A, A1, A2, A3, A4]= weights_age(newframe, oldframe, f_area, f_disp, f_orientation, eg, penalty, drift, kk);
    assignment= lapjv(A);
    
    toc
    
    %%
    tic
    l = length(newframe.cell_area);
    x = ismember(assignment,1:l);
    x_x = find(x==1);
    i=1;
    
    B= A;
    cost= NaN(l,1);
    while i <= numel(oldframe.cell_area)
        j= assignment(i);
        cost(j)= A(i,j);
        B(i,j)= inf;
        i=i+1;
    end
    % assignment2= lapjv(B); if intrested in second best assignment
    
    i=1;   
    cost1= NaN(l,1);
    while i <= numel(oldframe.cell_area)
        j= assignment(i);
        cost1(j)= A1(i,j);
        i=i+1;
    end
    i=1; 
    cost2= NaN(l,1);
    while i <= numel(oldframe.cell_area)
        j= assignment(i);
        cost2(j)= A2(i,j);
        i=i+1;
    end
    i=1;   
    cost3= NaN(l,1);
    while i <= numel(oldframe.cell_area)
        j= assignment(i);
        cost3(j)= A3(i,j);
        i=i+1;
    end    
    i=1;
    cost4= NaN(l,1);
    while i <= numel(oldframe.cell_area)
        j= assignment(i);
        cost4(j)= A4(i,j);
        i=i+1;
    end
    
    %keep old tracks which could not be assigned (not segmented or cost was too high)
    veryold = oldframe;
    SNames = fieldnames(oldframe);
    loop1=1;
    while loop1 <= numel(SNames)
        loop2= numel(x_x);
        while loop2> 0
            if strcmp((SNames{loop1}), 'cell_distances'); % Added for pombe
            veryold.(SNames{loop1}) = [];    
            else
                if loop2 < numel(x_x) % Mod for masked files 20161223
            veryold.(SNames{loop1})(x_x(loop2))= []; 
                end % Mod for masked files 20161223
            end
            loop2 = loop2-1;
            
        end
        loop1 = loop1+1;
    end
    
    %remove too old old tracks
    %-------------------------------------------------------------------------%
    % whole veryold story is obsolete, since tracker verification finds all
    % the unassigned tracks and handles them (not sure if this is always
    % true (fields with >1000 cells)
    %-------------------------------------------------------------------------%
    too_old = (find(veryold.age > maxjump));
    loop1 = 1;
    while loop1 <= numel(SNames)
        loop2= numel(too_old);
        while loop2> 0
            veryold.(SNames{loop1})(too_old(loop2))= [];
            loop2 = loop2-1;
        end
        loop1 = loop1+1;
    end
    
    
    % Displacement stored used in weight_age
    y_y = assignment(x_x);
    dif_x = zeros(l,1);
    dif_y = zeros(l,1);
    
    %---------------------------------------------------------------------%
    % Included Aarons filtered vector calculation (unchanged)
    % !!! has to be calculated or re calculated after get_TrackingVerification 
    % to correct new cell centers!
    % Calc again after tracker variation only for Nidx - y_y / x_x
    %-------------------------------------------------------------------------%
    dif_x(y_y) = (newframe.cell_center_x(y_y) - oldframe.cell_center_x(x_x))./oldframe.age(x_x); 
    dif_y(y_y) = (newframe.cell_center_y(y_y) - oldframe.cell_center_y(x_x))./oldframe.age(x_x);
    % Filter displacements (optional)
    if filter_support ~= 0
       filtered_dif_x = zeros(l,1);
       filtered_dif_y = zeros(l,1);
       [filtered_dif_x(y_y), filtered_dif_y(y_y)] = filter_vector_field(newframe.cell_center_x(y_y), ...
                                                                               newframe.cell_center_y(y_y), ...
                                                                               dif_x(y_y),...
                                                                               dif_y(y_y),...
                                                                               filter_support, ... % X
                                                                               filter_support);  % Y
    else
                filtered_dif_x = dif_x;
                filtered_dif_y = dif_y;
    end
   
    %
    veryold.age= veryold.age+1;
    
    new_trackindex = zeros(l,1);
    new_trackindex(y_y) = oldframe.trackindex(x_x);
    
    where = find(new_trackindex==0);
    add = length(where);
    new_trackindex(where)= numberoftracks+1:numberoftracks+add;
    
    %-------------------------------------------------------------------------%
    % Tracker verification starts here: using get_TrackingVerification
    % function.
    newframe.trackindex = new_trackindex; 
    newframe.dif_x = dif_x; 
    newframe.dif_y = dif_y; 
    newframe.filtered_dif_x = filtered_dif_x; 
    newframe.filtered_dif_y = filtered_dif_y; 
    [oldframe, newframe, veryold] = get_TrackingVerification( veryold, oldframe, newframe, kk, ImgFilePath, ImageFilesNames, 'TRUE', r);
    new_trackindex = newframe.trackindex;
    %-------------------------------------------------------------------------%
    
    % Assignin tracking results to original data struct
    % Assignin new trackindex
    data(framenumber).trackindex = new_trackindex;
    numberoftracks = numberoftracks + add;
    % ASSSIGNIN NEW COORDINATES!!!!
    data(framenumber).cell_center_x = newframe.cell_center_x;
    data(framenumber).cell_center_y = newframe.cell_center_y;
    data(framenumber).cell_area = newframe.cell_area; % needed to update data struct for miss segmented cases, otherwise would mess up in next round as misssegmentation would be template
    data(framenumber).cell_orientation = newframe.cell_orientation; % needed to update data struct for miss segmented cases, otherwise would mess up in next round as misssegmentation would be template
    data(framenumber).dif_x = newframe.dif_x;
    data(framenumber).dif_y = newframe.dif_y;
    data(framenumber).filtered_dif_x = newframe.filtered_dif_x;
    data(framenumber).filtered_dif_y = newframe.filtered_dif_y;
    data(framenumber).cell_distances = newframe.cell_distances; % added 16112016
    data(framenumber).qc_dist = newframe.qc_dist;
    data(framenumber).qc = newframe.qc;
    data(framenumber).qc_age = newframe.qc_age;
    data(framenumber).cell_del = newframe.cell_del;
    % assign costs
    data(framenumber).cost = cost(1:numel(new_trackindex));
    data(framenumber).cost1 = cost1(1:numel(new_trackindex));
    data(framenumber).cost2 = cost2(1:numel(new_trackindex));
    data(framenumber).cost3 = cost3(1:numel(new_trackindex));
    data(framenumber).cost4 = cost4(1:numel(new_trackindex));
    % assign displacement:
    %data(framenumber).displacement_x = dif_x(1:numel(new_trackindex));
    %data(framenumber).displacement_y = dif_y(1:numel(new_trackindex));
    
    %-------------------------------------------------------------------------%
    % Correct dimension missmatches between modified tracking results from
    % the get_TrackingVerification for all the remaining columns (struct
    % fields of the original data set. (Extending/Correction segmentation)
    %-------------------------------------------------------------------------%
    % Define fieldnames that should not be changed in size 
     StaticFieldNames = {'trackindex', 'cell_area', 'cell_orientation', 'cell_center_x', ...
        'cell_center_y', 'cost', 'cost1', 'cost2', 'cost3', 'cost4', 'qc', 'qc_dist', 'qc_age', 'dif_x', ...
        'dif_y', 'filtered_dif_x', 'filtered_dif_y', 'cell_distances', 'cell_del'};
    data = set_CorrectStructFieldDimensions(data, StaticFieldNames, framenumber);
    
    % Update oldframe
    oldframe.age = [ones(length(newframe.cell_area),1); veryold.age]; %[ones(l,1); veryold.age];
    oldframe.trackindex= [new_trackindex; veryold.trackindex];
    oldframe.cell_center_x= [newframe.cell_center_x; veryold.cell_center_x];
    oldframe.cell_center_y= [newframe.cell_center_y; veryold.cell_center_y];
    % change dif_x and dif_y to the size of the new frame
    oldframe.dif_x= [newframe.dif_x; veryold.dif_x];
    oldframe.dif_y= [newframe.dif_y; veryold.dif_y];
    % change filtered_dif_x and filtered_dif_y to the size of the new frame
    oldframe.filtered_dif_x= [newframe.filtered_dif_x; veryold.filtered_dif_x];
    oldframe.filtered_dif_y= [newframe.filtered_dif_y; veryold.filtered_dif_y];
    oldframe.cell_area= [newframe.cell_area; veryold.cell_area];
    oldframe.cell_orientation= [newframe.cell_orientation; veryold.cell_orientation];
    % change quality control to the size of the new frame
    oldframe.qc = [newframe.qc; veryold.qc];
    oldframe.qc_dist= [newframe.qc_dist; veryold.qc_dist];
    oldframe.qc_age= [newframe.qc_age; veryold.qc_age];
    oldframe.cell_del= [newframe.cell_del; veryold.cell_del];
    
      %-------------------------------------------------------------------------%
      % Delete Tracks that are marked for deletion (2) or delete data for
      % detected misssegmentation (1) using del_MisssegmentedData function.
      %-------------------------------------------------------------------------%
      FieldNamesToPreserve = {'trackindex', 'cell_area', 'cell_orientation', 'cell_center_x', ...
      'cell_center_y', 'cost', 'cost1', 'cost2', 'cost3', 'cost4', 'qc', 'qc_dist', 'dif_x', ...
      'dif_y', 'filtered_dif_x', 'filtered_dif_y', 'cell_distances'};
      %  data = del_MisssegmentedData( data, FieldNamesToPreserve, kk );
      if framenumber > 1
          data = del_MisssegmentedData( data, FieldNamesToPreserve, framenumber-1 );
      end
    
    kk = kk +1;
    toc
    
    disp(kk);
    assignin('base', 'datalive' , data)
end
end
