function  Data  = del_DataMeetsCriterion( Data, Criterion, Condition )
%del_DataMeetsCriterion Removes all entries meeting the condition of the
%criterion in each column of the struct data by first converting multi
%level structs to single level ones and then deleting the rows of the
%criterion meeting the condition. Then the struct is retransformed to its
%original level size. NOTE: delDataMeetsCriterionML is working without
%removing levels (works on logical fields with squareform whereas this
%function can not deal with the transformation of this data type.

% Input:
%   Data:                     Dataset [struct] With at least fields
%                             cell_index, cell_center_x and cell_center_y.
%   Criterion                 Fieldname [string] of Data struct that should
%                             meet the condition.
%   Condition:                [Array] Condition for the criterion to meet.
% Output:
%   Data:                     Same struct as input with Condition applied
%                             (data deleted).
% Usage:
%   Data =  del_DataMeetsCriterion( Data, Criterion, Condition );
%
% 20161221 Andreas Cuny

% Check if data struct is in the right format, otherwise convert it.    
SData = size(Data);
if SData(2) > 1
    DataStructML = Data;
    Data = set_MultiToSingleLevelStruct(Data, { });
end
FieldNames = fieldnames(Data);
Ind = ismember(Data.(Criterion),Condition);
% Remove the whole row, meeting the criterion condition
for n = 1:numel(FieldNames)
     Data.(FieldNames{n})(Ind,:) = [];
end

% Convert data struct back to its original size (levels)
if SData(2) > 1
    Data = set_SingleLevelStructToMultilevelSizeDiff( Data, DataStructML, FieldNames, Ind );
end

end

