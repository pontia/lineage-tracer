function  Data  = del_DataMeetsCriterionML( Data, Frame, Criterion, Condition )
%del_DataMeetsCriterion Removes all entries meeting the condition of the
%criterion in each column of the struct data
% Input:
%   Data:                     Dataset [struct] With at least fields
%                             cell_index, cell_center_x and cell_center_y.
%   Frame:                    [Array] Containing the number of the current
%                             frame or multiple frames (==struct level).
%   Criterion                 Fieldname [string] of Data struct that should
%                             meet the condition.
%   Condition:                [Array] Condition for the criterion to meet.
% Output:
%   Data:                     Same struct as input with Condition applied
%                             (data deleted [rows of criterion meeting the 
%                             condition]).
% Usage:
%   Data =  del_DataMeetsCriterionML( Data, Frame, Criterion, Condition  );
%
% 20161221 Andreas Cuny

% Check if data struct is in the right format.    
SData = size(Data);
if SData(2) > 1
    for f = 1:numel(Frame) % loop over array of frame
        FieldNames = fieldnames(Data);
        Ind = ismember(Data(Frame(f)).(Criterion),Condition);
        % Remove the whole row, meeting the criterion condition
        for n = 1:numel(FieldNames)
            if ~isempty(Data(Frame(f)).(FieldNames{n})(Ind,:))
                if size(Data(Frame(f)).(FieldNames{n}),1) == size(Data(Frame(f)).(FieldNames{n}),2) % check if data is in squareform
                    Data(Frame(f)).(FieldNames{n})(Ind,:) = []; % Handle squareform data rows
                    Data(Frame(f)).(FieldNames{n})(:,Ind) = []; % Handle squareform data columns
                else
                    Data(Frame(f)).(FieldNames{n})(Ind,:) = []; 
                end
            end
        end
    end
end

