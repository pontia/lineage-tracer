function [ data ] = del_MisssegmentedData( data, FieldNamesToPreserve, FrameNr )
%del_MisssegmentedData Removes all data in each column of the struct data
%except for user specified columns/fieldnames to preserve.
% Input:
%   data:                     Dataset [struct] With at least fields
%                             cell_index, cell_center_x and cell_center_y.
%   FieldNamesToPreserve      Array {cell} of fieldnames that have to be
%                             preserved. For the specified field names data 
%                             will not be removed
%   FrameNr:                  Current Frame number
% Output:
%   data:                     Same struct with new field qc, containing the
%                             unique hash/barcode value for each cell.
% Usage:
%   data =  del_MisssegmentedData( data, FieldNamesToPreserve, FrameNr );
%
% 20161105 Andreas Cuny

% To Do's: Introduce nargin check as well as check if fields 'trackindex'
%          and 'cell_del' are present in data struct.

% Nframes = numel(data);
FieldNames = fieldnames(data);
% Get difference in fields (only those names that should be deleted
delDataNames = setdiff(FieldNames, FieldNamesToPreserve);

% here remove data in the not user defined fields (columns)
% since the data is not to be trusted
for n = 1:numel(delDataNames)
    data(FrameNr).(delDataNames{n})(data(FrameNr).cell_del == 1) = NaN;
end

if ~isempty(intersect(data(FrameNr).cell_del, 2))
    % here remove the whole row, since it is a doublicate
    for n = 1:numel(FieldNames)
        % here remove the whole row, since it is a doublicate
        data(FrameNr).(FieldNames{n})(data(FrameNr).cell_del == 2) = [];
    end
end
end
% Alternative way how to delete rows in struct:
% data(1).(FieldNames)([data(1).(FieldNames)] == 1) = []
