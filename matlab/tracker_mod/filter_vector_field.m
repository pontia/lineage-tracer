function [ wdX, wdY ] = filter_vector_field( X, Y, dX, dY, radius_support_x, radius_support_y )
%filter_vector_field.m Filters a vector field by calculating a Gaussian-weighted average of neighboring
%     vector components at each position in the vector field.    
% 
%     The support of the Gaussian weighting kernel is defined as the radius of the 2D
%     Gaussian from the center to the location where the support falls to a 1% of the
%     central value.
% 
% Input:
%   X:                           list of X coordinates of the vector field.
%   Y:                           list of Y coordinates of the vector field.
%   dX:                          list of X components at each position in 
%                                the vector field.
%   dY:                          list of Y components at each position in 
%                                the vector field.
%   radius_support_x:            radius of the Gaussian weighting function 
%                                support in X direction
%   radius_support_y:            radius of the Gaussian weighting function
%                                support in X direction
%    
% Output:
%  wdX: filtered components ate the X positions with calculated Gaussian weights
%  wdY: filtered components ate the Y positions with calculated Gaussian weights.
%     
%    
% Written by Aaron Ponti
% 20161102, Translation to Matlab by Andreas Cuny

% To Do's: Insert vargin check 

% Make sure the dimensions are correct
    X = reshape(X, length(X), 1); 
    Y = reshape(Y, length(Y), 1); 
    dX = reshape(dX, length(dX), 1);
    dY = reshape(dY, length(dY), 1); 

    % Prepare the output
    wdX = zeros(size(X,1), 1);
    wdY = zeros(size(Y,1), 1);

    % Calculate sigma from the support
    sigma_x = radius_support_x / sqrt(2.0 * log(100));
    sigma_y = radius_support_y / sqrt(2.0 * log(100));

    for i = 1:length(X)

        % Calculate relative displacements around current position
        x0 = X - X(i);
        y0 = Y - Y(i);

        % Gaussian weight function - integral set to 1
        P = [x0, y0];
        W = mvnpdf(P, [0, 0], [sigma_x, sigma_y]); 
        W = W / sum(W);

        % Calculate weighted components wdX(i), wdY(i)
        wdX(i) = dot(dX', W);
        wdY(i) = dot(dY', W);
    end


end

