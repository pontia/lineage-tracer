function [ CellXTxtDataOut ] = get_CellXTxtData( PathNameTracking, FileNameTracking, NrowCellxTxt )
%get_CellXTxtData.m Loads the CellX segmenation results stored in
% text files (cells0000x.txt) into a cell struct.
% Input:
%   PathNameTracking:      Path to the CellX files (cells_*.txt)          
%   FileNameTracking:      File name of every timepoint
%   NrowCellxTxt:          Nr of heading columns (normally 35)
% Output:                  
%   CellXTxtDataOut:       Struct containing CellX Result data. Field names
%                          correspond column names of CellX result text files.
%
% Usage: get_CellXTxtData( PathNameTracking, FileNameTracking, NrowCellxTxt )
%
% 20160817, Andreas Cuny

% Preallocate for speed
CellXTxtData = [];
FieldNames = {};

% Get column names
fileID = fopen(fullfile(PathNameTracking, FileNameTracking{1}));
FieldNamesTemp = textscan(fileID,'%s', NrowCellxTxt, 'delimiter','\t');
fclose(fileID);

% Loop over all image frames (files)
for k = 1:NrowCellxTxt
FieldNames{k} = strrep(FieldNamesTemp{1,1}{k,1}, '.', '_');
end

for n = 1:numel(FileNameTracking)

% TODO: Working but not elegant!
%       Find better, automatic way to extract #columns 
fileID = fopen(fullfile(PathNameTracking, FileNameTracking{n}));
CellXTxtDataTemp = textscan(fileID,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ','delimiter','\t', 'HeaderLines' ,1);
fclose(fileID);

% Concentrate data in struct
for m =1:NrowCellxTxt
value = {CellXTxtDataTemp{1,m}};
CellXTxtData(n).(FieldNames{m})=value{1};
end
end

% TODO: Find a way how to concentrate cell array into one field instead of having
%       cells for each frames. But Attention if that would be changed.
%       addtrackindex_cunya.m needs to be changed accordingly!!!
% Note: set_MultiToSingleLevelStruct was written to achive that goal, but
%       it is easier to have all the frames in seperate cells/struct levels 
%       and concentrate only when needed e.g. Lineage reconstruction
for m =1:NrowCellxTxt
CellXTxtData(1).(FieldNames{m}) = CellXTxtData.(FieldNames{m});
end
CellXTxtDataOut = CellXTxtData;
end

