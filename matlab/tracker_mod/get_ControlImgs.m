function [ImgCont] = get_ControlImgs( CellsFilePath, IndexSelected, AxisHandle)
%get_ControlImgs.m This functions loads the CellX control and seeding
%images for the current frame (timepoint) for display in specified axes.
%
% CellsFilePath:        Absolute path to folder where control and seeding
%                       images are stored.
% IndexSelected:        Framenumber uf current selected frame. (Defined by
%                       listbox selection.
%
%   20161003 Andreas Cuny

ControlImg = fullfile(CellsFilePath, [sprintf('control_%05d', IndexSelected), '.png']);
[img1,map1]  = imread(ControlImg, 'png');
x1rgb = ind2rgb(img1, map1);
%imshow(x1rgb)
SeedingImg = fullfile(CellsFilePath, [sprintf('seeding_%05d', IndexSelected), '.png']);
[img2,map2]  = imread(SeedingImg, 'png');
x2rgb = ind2rgb(img2, map2);
%imshow(x2rgb)
axes(AxisHandle)
ImgCont = imshow(0.3.*x2rgb + x1rgb );
end

