function [ data ] = get_QualityControlT( data, ImgFilePath, ImageFilesNames,  r, Cell2Correct )
%get_QualityControl Introduces a unique cell track quality control feature 
%based on an Out Of Focus Brightfield image using the DCT transform. 
%   Here the quality control features (hash/barcode) are computed for each
%   frame using DCT and binarization of the frequencies containing the structural
%   information creating a unique hash. It is possible to only calculate
%   the qc feature for a number of specified tracks in a frame.
%
% Input:
%   data:                     Dataset [struct] With at least fields
%                             cell_index, cell_center_x and cell_center_y.
%   ImgFilePath:              Path [String] to directory of out-of-focus
%                             Brightfield images.
%   ImageFilesNames:          Brightfield images [Cell] filenames.
%   r:                        Radius [pixel] defining crop region around each 
%                             cell center. 
%   Cell2Correct:             Index of a cell in a frame where qc has to be
%                             re-calculated.
% Output:
%   data:                     Same struct with new field qc, containing the
%                             unique hash/barcode value for each cell.
% Usage:
%   data = get_QualityControl( data, ImgFilePath, ImageFilesNames, r );
%
% 20161012 Andreas Cuny, 201610125 Modified for get_TrackerVerification.
    
tic
warning('off', 'all')

% Names = fieldnames(data.CellXTxtDataTracked);
% Tmp = set_MultiToSingleLevelStruct(data.CellXTxtDataTracked, Names(1));
% BinaryStrings = cell(1, numel(Tmp));
%CellXTxtDataTracked = data.CellXTxtDataTracked;

NFrames = numel(data); 

textprogressbar('Calculating independent tracker quality control features: ', NFrames);
for n = 1:NFrames
    textprogressbar(n, NFrames);
    % Read image of current frame
    Img_Full = imread(fullfile(ImgFilePath, ImageFilesNames{n})); % path to greyscale image
    
    % write function independent of this. such that only data index are
    % used!
    CellIdxIdent = {'cell_index', 'trackindex'};
    if isfield(data, CellIdxIdent{1})
        CellIdx = data(n).(CellIdxIdent{1}); % Rethink that part: has to be a fieldname that is in all kind of dataframes.
        NCellIdx= numel(CellIdx);
        CellIdxIdent = CellIdxIdent{1};
    elseif isfield(data, CellIdxIdent{2})
        CellIdx = data(n).(CellIdxIdent{2}); 
        NCellIdx= numel(CellIdx);
        CellIdxIdent = CellIdxIdent{2};
    else
        error('No field cell_index or trackindex found in your data. Function needs an index for each cell')
    end
    
    if nargin == 4
        FromCellIdx = 1;
    elseif nargin == 5
        FromCellIdx = find(CellIdx == Cell2Correct);
        NCellIdx = find(CellIdx == Cell2Correct);
    else
        error('Please check number of arguments')
    end

  for m = FromCellIdx:NCellIdx
        
        center1x= data(n).cell_center_x(data(n).(CellIdxIdent)==CellIdx(m)); 
        center1y= data(n).cell_center_y(data(n).(CellIdxIdent)==CellIdx(m)); 

        % Crop image to specified region around cell of current track
        Crop_Coords= [center1x-r,center1y-r, 2*r,2*r];
        
        if numel(Crop_Coords) == 4 
        Img = imcrop(Img_Full, Crop_Coords);
        % Do transformation
        %-----------------------------------------------------------------%
        % Resize image to 32x32
        I_1= Img;
        I2_1 = imresize(I_1, 1/(size(Img,1)/32));
        % Cosine transform
        d_1 = dct2(I2_1);
        %d = d(1:8,1:8);
        d2_1 = d_1(1:8,1:8);
        % Compute average of transform
        a_1 = mean(mean(d2_1));
        % Set final bits
        b_1 = d2_1 > a_1;
        % Convert to string:
        % Save binary string for all tracks and frames in matrix
        data(n).qc(data(n).(CellIdxIdent)==CellIdx(m)) = cellstr(num2str(b_1(:)'));
        assignin('base', 'string_1', data)

        % Idea instead of saving into full array save into new column of
        % multifield struct.
        % Idea2 convert matrix to sparse matrix and see how performance
        % improves.
        else
        end
  end
end
textprogressbar('Done', NFrames);
toc
end