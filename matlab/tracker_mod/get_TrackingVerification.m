function [ oldframe, newframe, veryold ] = get_TrackingVerification( veryold, oldframe, newframe, kk, ImgFilePath, ImageFilesNames, FilterForNeighbours, radius)
% get_TrackingVerification.m Runs a segmentation independent tracking quality
% control. Based on the out come a possible proposed track is kept, changed
% or deleted. A track is kept if QC is positive. A track is changed if the
% segmentation was bad ( cell center and trackindex will be kept but other data
% will be deleted) or if tracker jupmped to another cell. Here data will be
% relinked.
%   Important: Run in the end a check of how many cells index we have vs how
%   many tackindex and try to assing missing cells by comparing those cell
%   centers with their previous cells (but only with neighbours for higher
%   accuracy)
%
% Input:
%   veryold:                    Data [Struct] of trackers veryoldframe containing
%                               fields: 'age','trackindex','dif_x','dif_y',
%                               'cell_center_x','cell_center_y','cellarea',
%                               'cellorientation'
%   oldframe:                   Data [Struct] of trackers oldframe containing
%                               fields: 'age','trackindex','dif_x','dif_y',
%                               'cell_center_x','cell_center_y','cellarea',
%                               'cellorientation', 'qc', 'qc_dist',
%                               'qc_age', 'cell_del'
%   newframe:                   Data [Struct] of trackers oldframe containing
%                               fields: 'cell_center_x','cell_center_y','cellarea',
%                               'cellorientation', 'qc', 'qc_dist', 'qc_age', 'cell_del'
%   kk:                         Current frame number [Int]
%   ImgFilePath:                Path [String] to directory of out-of-focus
%                               Brightfield images.
%   ImageFilesNames:            Brightfield images [Cell] filenames.
%   FilterForNeighbours:        [Boolean] If set to 'TRUE' quality control of
%                               oldframe is only compared to cells within 
%                               cell_distance. Else 'FALSE' compares qc to
%                               all cell ( less accurate, more false positives ) 
%   radius:                     Radius [pixel] defining crop region around each 
%                               cell center. 
%
% Output:
%   data:                       Data [Struct] (not needed, will output
%                               corrected veryold, oldframe and newframe
%
% Usage:
% [ oldframe, newframe, veryold ] = get_TrackingVerification( veryold, oldframe, newframe, kk, ImgFilePath, ImageFilesNames, FilterForNeighbours, radius)
%
% 20161021, Andreas Cuny


Tracks_All = oldframe.trackindex;
NTracks = numel(Tracks_All);
% Preallocation
PredictedCellTrack = cell(NTracks, 4);

%---------------------------------------------------------------------%
% Loop over all tracks in old frame and find the same cell in the newframe
% according to the qc feature.
for l = 1:NTracks
    % Get fingerprint of trackindex of interest in oldframe
    a = oldframe.qc(oldframe.trackindex == Tracks_All(l));
    
    if strcmp(FilterForNeighbours, 'TRUE') == 0
        % Get fingerprint of all tracks in the next frame (newframe)
        b = newframe.qc;
    else
        % Instead of comparing each cell in oldframe to all cells in newframe, 
        % compare them only to direct neighbours (distance (rn) around cell center to include
        % more/less neighbours can be chosen individually. Has to be run
        % beforehand in addtrackindex_cunyaT creating new field
        % cell_distances)
        b_temp = newframe.cell_distances(: , newframe.trackindex == Tracks_All(l));
        b_temp(find(newframe.trackindex == Tracks_All(l))) = 1; % add current track to the selection list as well
        b = newframe.qc(b_temp);
    end

    % Compare fingerprints
    temp = zeros(1,numel(b));
    for f = 1:numel(b)
        temp(1,f) = pdist2(cell2mat(a(1)),cell2mat(b(f)), 'hamming'); % The blank spaces between bits are also considered in the calculation!
    end
    
    % Find the closest fingerprint to the cell of interest (COI) in oldframe 
    % and  check if we can find/predict the trackindex of the correct cell
    % in the newframe
    % min : determines match (as long as below 0.1, otherwise misssegmentation)
    idx_predicted_cell = find(temp == min(temp));
    if strcmp(FilterForNeighbours, 'TRUE') == 0
        PredictedCellTrack{l,1} = Tracks_All(l);
        PredictedCellTrack{l,2} = newframe.trackindex(idx_predicted_cell);
    else % For neighbours only
        possibletracks = newframe.trackindex(b_temp);
        %if all(temp > 0.1)    % Here I have already a thresholding!! Remove that and then check later for misssegmentation!
        idx_predicted_cell = find(possibletracks == Tracks_All(l)); % changed l to Tracks_All(l)
        PredictedCellTrack{l,4} = possibletracks;
        %end
        PredictedCellTrack{l,2} = possibletracks(idx_predicted_cell);
        PredictedCellTrack{l,1} = Tracks_All(l);
    end
    PredictedCellTrack{l,3} = temp; 
    % Note: PredictedCellTrack has to be written out in the end either to 
    % newframe/oldframe structs or to 's' directly. 
    % * [ ] We must be able to load it later again to check for other candidates
    newframe.qc_dist{l} = temp(idx_predicted_cell); % * [x] Output relevant distance between oldframe.qc and newframe.qc that was used to qualify the trackaccuracy
end  
%---------------------------------------------------------------------%

    %---------------------------------------------------------------------%
    % Tracking verification starts here: 
    % Note: messages is a table for sanity checks only
    Nnewframe = numel(newframe.trackindex);
    for f = 1:NTracks
 
    l = cell2mat(PredictedCellTrack(f,1));
    r = cell2mat(PredictedCellTrack(f,2));
    messages{f,2} = l;
    if r == l % tracker was correct do nothing 
        % * [x] option check segmentation by looking at past pdistances -> store them if deviation is to big bad segmentation
        % * [ ] option2 check area how much increased if to much bad segmentation. delete all information
        if  newframe.qc_dist{f} < 0.1 %&& ~isepmty(newframe.qc_dist{f}); % Tracker ok
            disp('tracker ok') % No interaction needed, segmentation is ok.
            messages{f,1} = 'tracker ok';
            messages{f,3} = r;
        else % Tracker ok but misssegmentation, since qc_dist above threshold
             % *[ ] Option to be implemented: double check with qc_dist history if value
             %      changed a lot, indication for misssegmentation.
            disp('tracker ok but bad/miss segmentation. Delete Data')
            messages{f,1} = 'tracker ok  but bad/miss segmentation. Delete Data';
            messages{f,3} = r;

            %/-----------------------------------------------------------%
            % Delete datapoints: * [x] write function to find that cell in the
            % data function and delete all other infos as well! Introduce
            % new field cell_delete. If 1 data must be deleted but tracking
            % information will be keept (coordinates, trackindex etc).
            % del_MisssegmentedData.m function.
            
            % Here copy area and orientation from previous frame just to
            % keep the tracker running, BUT DO NOT store the copied area
            % and orientation into the original data struct. Then this
            % would contradict the misssegmentation. This info needs to be
            % deleted after the tracking (with del_MisssegmentedData.m)! 
            %(cell_del = 1)

            newframe.trackindex(newframe.trackindex == r) = l; % not really needed since r == l
            messages{f,4} = l;
            % Move cell center to right position:
            % * [ ] Here it would be better to use the coordinates
            % corrected by the filtered_dif! Not implemented
            newframe.cell_area(newframe.trackindex == r) = oldframe.cell_area(oldframe.trackindex == l);
            newframe.cell_orientation(newframe.trackindex == r) = oldframe.cell_orientation(oldframe.trackindex == l);
            newframe.cell_center_x(newframe.trackindex == r) = oldframe.cell_center_x(oldframe.trackindex == l);
            newframe.cell_center_y(newframe.trackindex == r) = oldframe.cell_center_y(oldframe.trackindex == l);
            newframe.dif_x(newframe.trackindex == r) = oldframe.dif_x(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
            newframe.dif_y(newframe.trackindex == r) = oldframe.dif_y(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
            newframe.filtered_dif_x(newframe.trackindex == r) = oldframe.filtered_dif_x(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
            newframe.filtered_dif_y(newframe.trackindex == r) = oldframe.filtered_dif_y(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
           %  newframe.cost(newframe.trackindex == r) = oldframe.cost(oldframe.trackindex == l);
           %  newframe.cost1(newframe.trackindex == r) = oldframe.cost1(oldframe.trackindex == l);
           %  newframe.cost2(newframe.trackindex == r) = oldframe.cost2(oldframe.trackindex == l);
           %  newframe.cost3(newframe.trackindex == r) = oldframe.cost3(oldframe.trackindex == l);
           %  newframe.cost4(newframe.trackindex == r) = oldframe.cost4(oldframe.trackindex == l);
            % Recalculate qc now at copied position (ATTENTION it might be
            % better not to copy the cell center from the previous frame
            % but use the corrected cell center by filtered dif
            newframe2 = get_QualityControlT( newframe, ImgFilePath, ImageFilesNames,  radius, l ); 
            %/ Sanity check
%             assignin('base', 'oldframe', oldframe)
%             assignin('base', 'newframe', newframe)
%             assignin('base', 'l', l)
%             assignin('base', 'r', r)
%             assignin('base', 'Nnewframe', Nnewframe);
%              assignin('base', 'messages', messages);
%              assignin('base', 'f', f);
%              assignin('base', 'PredictedCellTrack', PredictedCellTrack);
            %/
            % Update qc_dist as well
            newframe2.qc_dist{newframe2.trackindex == r} = pdist2(cell2mat(oldframe.qc(oldframe.trackindex == l)),cell2mat(newframe2.qc(newframe2.trackindex == l)), 'hamming');
            newframe2.qc_age(newframe2.trackindex == r) = oldframe.qc_age(oldframe.trackindex == l) +1;
            % Misssegmentation data must be deleted
            newframe2.cell_del(newframe2.trackindex == r) = 1;
            newframe = newframe2;
            %------------------------------------------------------------/%
        end
        
    elseif size(r, 1) > 1 % size(PredictedCellTrack{f,2}, 1) > 1 
        % More than one trackindex was found/predicted, check if same
        % trackindex as in oldframe can be found. If so use that one only,
        if ~isempty( find(PredictedCellTrack{f,2} == l))
             TCand = PredictedCellTrack{f,2};
             TempTIdx = find(PredictedCellTrack{5,2} == TCand);
             PredictedCellTrack{f,2} = TCand(TempTIdx);
             sprintf('more than one solution%d and trackpos %d' , l, f)
        else
        % * [ ] Option: otherwise calc distance of cell centers of found matches to
        %               cellcenter of oldframe to find closest one.
        % * [ ] If there is no match in both cases delete point and copy l
        end
        
    else % Tracker failed, now check first if a prediction could be found from the overyold. 
         % If so assign oldframe track to newframe track (then check if that track is in the veryold and remove it)
           
        if isempty(r)
            % * [x] r is empty extract coordinates from l and write them to newframe
            %       under the same trackindex. there is no cell under theese
            %       coordinates so we need to plug in the field infos NAN
            %       directly into data struct to keep order and matrix size.
            % * [ ] Write function to do that to fill in new rows at right row
            %       index.
             disp('r is empty, NO segmentation set new cell center and copy trackindex from l')
             messages{f,1} = 'r is empty, NO segmentation set new cell center and copy trackindex from l';
        
             PredictedCellTrack{f,2} = l;
             messages{f,4} = l;
             %/-----------------------------------------------------------%
             % Get coordinates of previous frame. Better use those together
             % with weighted displacement for more accurate cell center
             % prediction / especially for later resegmentation.
             Nnewframe = Nnewframe + 1;
             newframe.trackindex(Nnewframe) = l;
             messages{Nnewframe,3} = l;
             newframe.cell_area(Nnewframe) = oldframe.cell_area(oldframe.trackindex == l);
             newframe.cell_orientation(Nnewframe) = oldframe.cell_orientation(oldframe.trackindex == l);
             % Attention: It makes more sense to instead of using the
             % cell position to use the filtered and predicted one
             % HERE one could also use veryold frame directly
             newframe.cell_center_x(Nnewframe) = oldframe.cell_center_x(oldframe.trackindex == l); %+ oldframe.filtered_dif_x(oldframe.trackindex == l);
             newframe.cell_center_y(Nnewframe) = oldframe.cell_center_y(oldframe.trackindex == l); %+ oldframe.filtered_dif_y(oldframe.trackindex == l);
             newframe.dif_x(Nnewframe) = oldframe.dif_x(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
             newframe.dif_y(Nnewframe) = oldframe.dif_y(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
             newframe.filtered_dif_x(Nnewframe) = oldframe.filtered_dif_x(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
             newframe.filtered_dif_y(Nnewframe) = oldframe.filtered_dif_y(oldframe.trackindex == l); % needed to keep dimensions right for weights_age function
             % newframe.age(Nnewframe) = 1; % not needed
             % not elegant calculates it every time for all, rewrite such that only for particular row stuff is calculated
             % newframe = get_QualityControlT( newframe, ImgFilePath, ImageFilesNames,  radius );
             %/ Sanity check
%              assignin('base', 'newframe', newframe);
%              assignin('base', 'oldframe', oldframe);
%              assignin('base', 'veryold', veryold);
%              assignin('base', 'l', l);
%              assignin('base', 'r', r);
%              assignin('base', 'Nnewframe', Nnewframe);
%              assignin('base', 'messages', messages);
%              assignin('base', 'f', f);
%              assignin('base', 'PredictedCellTrack', PredictedCellTrack);
            %/ 
             disp(kk)
             newframe = get_QualityControlT( newframe, ImgFilePath, ImageFilesNames,  radius, l );
             %newframe.qc{Nnewframe, 1} = cell2mat(oldframe.qc(oldframe.trackindex == l)); % Temporarly solution just copying the qc from the frame before. not elegant call function here to extract features again.
             %newframe.qc_dist{Nnewframe} = NaN;
             newframe.qc_dist{Nnewframe} = pdist2(cell2mat(oldframe.qc(oldframe.trackindex == l)),cell2mat(newframe.qc(newframe.trackindex == l)), 'hamming');
             
             newframe.qc_age(Nnewframe) = oldframe.qc_age(oldframe.trackindex == l) +1;
             % Assign 1 since no segmentation data exists
             newframe.cell_del(Nnewframe) = 1;
              
             
             % newframe.cell_distances(Nnewframe) = NaN;
             % Delete all remaining datapoints (write function to find that cell in the
             % data function and delete all other infos as well!
             %------------------------------------------------------------/%
        else % r is not empty but also not equal than oldframe. Now check first if segmentation was good.
            % if so assign oldframe track to newframe track if not delete data in newframe
            disp('r is NOT empty')
            % * [ ] Solve begining value problem. no qc_dist history in first frame
            %       therefore area needed. (or myabe not :))
            % * [x] Area data can be used but should be avoided to keep qc
            %       independent of segmentation.
            OldArea = oldframe.cell_area(oldframe.trackindex == l); 
            NewArea = newframe.cell_area(newframe.trackindex == l); % Important, has to be l, then r might be another cell. but l might not exist catch this case as well    
            if  newframe.qc_dist{f} > 0,1; % NewArea >= OldArea * 1.2 && % bad segmentation delete data optional re-segment
                disp('r is NOT empty but bad/miss segmentation. Delete Data')
                messages{f,1} = 'r is NOT empty but bad/miss segmentation. Delete Data';
                messages{f,3} = r;
                % *[ ] Option: double check with qc_dist history if value
                %      changed a lot, indication for misssegmentation.
             %/-----------------------------------------------------------%   
             
             % Here copy area and orientation from previous frame just to
             % keep the tracker running, BUT DO NOT store the copied area
             % and orientation into the initial data struct. Then this
             % would contradict the misssegmentation. this info needs to be
             % deleted.
             %newframe.trackindex(newframe.trackindex == r) = l;
             messages{f,4} = l;
             messages{f,4} = r;
             newframe.cell_area(newframe.trackindex == r) = oldframe.cell_area(oldframe.trackindex == l); 
             newframe.cell_orientation(newframe.trackindex == r) = oldframe.cell_orientation(oldframe.trackindex == l); 
             % What should we do with those, since it is apparently
             % wrong... or might be missleading later. I tend to delete,
             % with option to resegment here as well.
             %newframe.qc{Nnewframe} = NaN;
             %newframe.qc_dist{Nnewframe} = NaN;
             %newframe.cell_distances(Nnewframe) = NaN;
             % Move cell center to right position:
             newframe.cell_center_x(newframe.trackindex == r) = oldframe.cell_center_x(oldframe.trackindex == l);
             newframe.cell_center_y(newframe.trackindex == r) = oldframe.cell_center_y(oldframe.trackindex == l);
             %
             newframe.qc_age(newframe.trackindex == r) = oldframe.qc_age(oldframe.trackindex == l);
             % Assign 2 since miss segmentation data can not be trusted
             newframe.cell_del(newframe.trackindex == r) = 1;
             %------------------------------------------------------------/%
                
                
            else % Segmentation good but trackindex not identical with qc [jumped cell, qc false positive];
                disp('r is NOT empty, but tracker jumpted cell or qc false positive. Relink with trackindex old/ new with min cell coord dist.')
                messages{f,1} = 'r is NOT empty, but tracker jumpted cell or qc false positive. Relink with trackindex old/ new with min cell coord dist.';
                
                % Check first if it is not a tracker verification FP! by
                % checking if for f there is r == l
                Idx_l = find(ismember(cell2mat(PredictedCellTrack(:,1)), r));
                PredTrack = cell2mat(PredictedCellTrack(Idx_l,2));
                if ~isempty(Idx_l) % find better solution to solve && issue if both parts are empty!
                if PredTrack == r && Idx_l ~= r % Means that predicted track r in newframe for oldframe track l is present in oldframe
                    % and was correctly found in newframe but at another
                    % index. (TrackerVerification False Positive). Therefore infromation from oldframe l has to
                    % be copied into newframe if l can not be found in
                    % newframe at another index.
                else
                % * [x] Compare eucledian distance of cell centers as
                %       doublecheck first if new track should be assigned
                %       or if its a false positive
                xl = oldframe.cell_center_x(oldframe.trackindex == l);
                yl = oldframe.cell_center_x(oldframe.trackindex == l);
                xr = newframe.cell_center_x(newframe.trackindex == r);
                yr = newframe.cell_center_x(newframe.trackindex == r);
                center_dist = pdist2([xl, yl], [xr, yr]);
                if center_dist < 8 % find out what value is correct. Probably this should not be HardCoded since it changes from species to species
                    %/-----------------------------------------------------------%
                    % Correct trackindex and all other data at that position in
                    % the newframe struct:
                    newframe.trackindex(newframe.trackindex == l) = r;
                    messages{newframe.trackindex == l,3} = r;
                    % newframe.age(f) = newframe.age(newframe.trackindex ==
                    % r); % not needed
                    newframe.cell_center_x(newframe.trackindex == l) = newframe.cell_center_x(newframe.trackindex == r);
                    newframe.cell_center_y(newframe.trackindex == l) = newframe.cell_center_y(newframe.trackindex == r);
                    newframe.cell_area(newframe.trackindex == l) = newframe.cellarea(newframe.trackindex == r);
                    newframe.cell_orientation(newframe.trackindex == l) = newframe.cellorientation(newframe.trackindex == r);
                    newframe.qc{newframe.trackindex == l} = newframe.qc(newframe.trackindex == r);
                    newframe.qc_dist{newframe.trackindex == l} = newframe.qc_dist(newframe.trackindex == r);
                    % Assign 0 since segmentation should be correct and
                    % data can be keept
                    newframe.cell_del(newframe.trackindex == l) = 0;
                %    newframe.cell_distances(f) = newframe.cell_distances(newframe.trackindex == r);
                    %------------------------------------------------------------/%
                end
                end
                end

            end
        end
    end

    end
   
    %---------------------------------------------------------------------%
    % To Do's:
    % * [x] get somehow trackindex field into old as well new frame. or do
    % quality check just right after that, before writing it into
    % addtrackindex struct 's'.
    
    % * [x] veryold holds tracks that could not be assigned or are not segmented.
    % check those as well with qc if and where they fit in
    
    % * [ ] build difference between cell centers of tracks vs cell center of no
    % tracks and check if those cells can be assigned to a track (they
    % should be in the old and newframe structs (same size)
    % * [ ] introduce same as tracker field 0 == no, 1 == true
    
    % * [x] write function to calc cell center diff. Check with aarons
    % filtering method do get it more accurate. Store the difs also in the
    % newframe. initialize first the fileds in the struct.
    
    % * [x] Find out how to copy, fill the cell distance matrix that it matches the size. 
    % Option recalc pdist, write as function call. 
    
    % * [x] End tracks. Tracks need to be ended if cell is lost (out of FOV) or
    % if it is not a cell (misssegmentation false positive FP)
    % * [x] recognize FP by non changing cell_area for several frames. In every
    % frame where the cell_area did not change to the previous frame the age
    % increases by 1. If age is larger than 3 (maxjump) track will be
    % deleted.
    
    %---------------------------------------------------------------------%
    % Stuff to do after TrackingVerification
    %---------------------------------------------------------------------%
    
    % Get difference between veryold and newframe. Idealy all veryold frame
    % information should now be contained in newframe completly
    % veryold can be returned completely empty and will still work.
    [C, vdif_idx] = intersect(veryold.trackindex, newframe.trackindex);
    veryoldNames = fieldnames(veryold);
    for t = 1:numel(veryoldNames)
        if strcmp(veryoldNames{t}, 'cell_distances'); % Added for pombe
        veryold.(veryoldNames{t}) = [];    
        else
       % if vdif_idx < numel(veryold.age) % Mod Masked Data 20161223
        veryold.(veryoldNames{t})(vdif_idx) = [];
      %  end % Mod Masked Data 20161223
        end
    end
    
    % Recalculate neighbours matrix to correct row dimension of newly added
    % tracks
    Coords = [newframe.cell_center_x, newframe.cell_center_y];
    CellDist = pdist(Coords) <= radius;
    newframe.cell_distances = squareform(CellDist);
    
    %---------------------------------------------------------------------%
    % This should never happen with the current implementation!!!
    %---------------------------------------------------------------------%
    % check for double trackindex and mark row for deletion
    [n, bin] = histc(newframe.trackindex, unique(newframe.trackindex));
    doublicates = find(n > 1);
    doublicates_idx = find(ismember(bin, doublicates));
     
    if ~isempty(doublicates)
        disp('-----------------------DOUBLICATES FOUND--------')
        dn_x = newframe.cell_center_x(doublicates_idx);
        dn_y = newframe.cell_center_y(doublicates_idx);
        dn = [dn_x, dn_y];
        do_x =  oldframe.cell_center_x(oldframe.trackindex == unique(oldframe.trackindex(ismember(bin, doublicates))));
        do_y =  oldframe.cell_center_y(oldframe.trackindex == unique(oldframe.trackindex(ismember(bin, doublicates))));
        do = [do_x, do_y];
        del_idx = doublicates_idx(dsearchn(dn, do));
        newframe.cell_del(del_idx) = 2;
        
    end
    
    % Find to old tracks and mark them for complete deletion.
        too_old = (find(newframe.qc_age > 4)); % >maxjump
    %   newframe.cell_del(too_old) = 2; % 2 equals complete track deletion
    
end

