function [ data ] = set_CorrectStructFieldDimensions( data, FieldNamesToPreserve, FrameNr)
%set_CorrectStructFieldDimensions Corrects length differences between data
%columns changed during tracker verification and the rest by filling in NaNs.
% Input:
%   data:                     Dataset [struct] With at least fields
%                             cell_index, cell_center_x and cell_center_y.
%   FieldNamesToPreserve      Array {cell} of fieldnames that have to be
%                             preserved. For the specified field names row 
%                             data will not be added.
%   FrameNr:                  Current Frame number
% Output:
%   data:                     Same struct with new field qc, containing the
%                             unique hash/barcode value for each cell.
% Usage:
%   data =  set_CorrectStructFieldDimensions( data, FieldNamesToPreserve, FrameNr );
%
% 20161115 Andreas Cuny

% Get fieldnames of data struct
FieldNames = fieldnames(data);
% Get difference in fields (only those names that should be altered)
corrDataNames = setdiff(FieldNames, FieldNamesToPreserve);
% Get length difference between trackindex and an unchanged column
Ldif = numel(data(FrameNr).trackindex) - numel(data(FrameNr).cell_frame);

% Correct length of remaining fieldnames
for n = 1:numel(corrDataNames)
    % Extend the so far unchanged columns be NaN to get the dimensions correct.
    data(FrameNr).(corrDataNames{n}) = [data(FrameNr).(corrDataNames{n}); NaN(Ldif, 1)];
end

end

