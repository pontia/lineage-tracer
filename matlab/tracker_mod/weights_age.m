function [A, A1, A2, A3, A4] = weights_age(newframe, oldframe, f_area, f_disp, f_orientation, eg, penalty, drift, kk)
% function to calculate the individual costs
% in here, the penalty for age is hard coded
% kk is there for test purposes, such that oe can stop wherever one wants
penalty= penalty/4; % average of 4 factors
ro = length(oldframe.cell_area); % dim of rows oldframe
cn = length(newframe.cell_area); % dim of cols newframe

% calculation of x - y pixel position with the added drift; in other words
% we calculate the expected position in the new frame based on the position
% in the old frame
distance_x = repmat(oldframe.cell_center_x+drift*oldframe.age.*oldframe.dif_x,1,cn) - repmat(newframe.cell_center_x.',ro,1);
distance_y = repmat(oldframe.cell_center_y+drift*oldframe.age.*oldframe.dif_y,1,cn) - repmat(newframe.cell_center_y.',ro,1);

% average area of all cells in the respective frame ...  the idea is that
% one could change the growth pattern based on the size.
mean_area = mean(newframe.cell_area);

% check what are young and old cells based on the area. Young cells are all the ones which are smaller than the average size. It is a current
% idea but not finished yet.
sum_area = repmat(newframe.cell_area.',ro,1) + repmat(oldframe.cell_area,1,cn);
young_cells = find(sum_area <mean_area);
rel_area= repmat(newframe.cell_area.',ro,1)./repmat(oldframe.cell_area,1,cn);

% calcualte the difference in cell orientation
dif_orientation = repmat(oldframe.cell_orientation,1,cn) - repmat(newframe.cell_orientation.',ro,1);

% calculation of the different penalties (cost)
% calculation of the x - y displacement penalty on the predicted position
A1= f_disp*(distance_x.^2 + distance_y.^2);
A1= [A1 repmat(penalty,ro)];

% penalty on area, different for old and young cells - the young cells have
% a 100 fold lower weight
A2= f_area*((log(rel_area)-eg).^2);
A2(young_cells)= A2(young_cells)/100;
A2= [A2 repmat(penalty,ro)];

% function as before - goal is when they rotate much
A3= f_orientation*(sin(pi/180*dif_orientation)).^2;
A3= [A3 repmat(penalty,ro)];

% penalty for frame skipping - 200 per skipped frame
A4= 200*repmat(oldframe.age-1,1,cn);
A4= [A4 repmat(penalty,ro)];

A = A1 + A2 + A3 + A4; 
assignin('base', 'A', A)
end