DEFAULT_FACTOR_AREA = 5e3
DEFAULT_FACTOR_ORIENTATION = 0
DEFAULT_DISPLACEMENT_PENALTY = 2.0
DEFAULT_GROWTH_PENALTY = 0.03
DEFAULT_MAX_PENALTY = 2e3
DEFAULT_DRIFT = 0.7
DEFAULT_JUMP_PENALTY = 200
DEFAULT_MAX_JUMP = 2
DEFAULT_FILTER_SUPPORT = 100
DEFAULT_EXPORT = False
DEFAULT_QC = False
DEFAULT_NUMBER_OF_FILES = -1
