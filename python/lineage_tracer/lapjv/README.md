This folder contains three implementations of the Jonker-Volgenant Algorithm for solving linear assignment problems. Use of the first two or the three algorithms described below is governed by a boolean flag in ```analysis/tracker.py```.

	R. Jonker and A. Volgenant, "A shortest augmenting path algorithm for dense and spare linear assignment problems", Computing, Vol. 38, pp. 325-340, 1987.

### ```lapjv_cython()``` (fast and default)

Implemented in ```lapjv2.py``` as a wrapper around ```lapjv.py``` and ```_lapjv.pyx```, the python + cython implementation from the CellProfiler project (http://cellprofiler.org) and built into a shared library (a ```_lapjv.pyd``` build for Windows is committed to the repository).

```python
from lapjv import lapjv2
sol = lapjv2.lapjv_cython(cost_matrix)
```

Used when ```__DEBUG_USE_LAPJV2_PYTHON__ = False``` at the top of ```analysis/tracker.py```.

### ```lapjv_python()``` in ```lapjv2.py``` (slow)

Implemented in ```lapjv2.py``` as a pure python + numpy re-implementation of the Matlab version by Yi Cao at Cranfield University (http://ch.mathworks.com/matlabcentral/fileexchange/26836-lapjv-jonker-volgenant-algorithm-for-linear-assignment-problem-v3-0).

```python
from lapjv import lapjv2
sol = lapjv2.lapjv_python(cost_matrix)
```

Used when ```__DEBUG_USE_LAPJV2_PYTHON__ = True``` at the top of ```analysis/tracker.py```.

### ```lapjv()``` in ```_lapjv2.pyx```

This was a quick (and partial) attempt to cythonize ```lapjv_python()```. It provides performance half-way between ```lapjv_python()``` and ```lapjv_cython()``` and is preserved, just in case.

### How to build _lapjv.pyd (and _lapjv2.pyd)

To build ```_lapjv.pyd``` (and ```_lapjv2.pyd```) follow the instructions in ```lapjv/setup.py```.

### Also interesting

https://github.com/hrldcpr/pyLAPJV
