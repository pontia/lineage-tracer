import numpy as np
from lineage_tracer.lapjv import lapjv

__author__ = 'Aaron Ponti'


def lapjv_cython(cost_mat, resolution=None, calc_reduced_cost_matrix=False):
    """
    Jonker-Volgenant Algorithm for Linear Assignment Problem.

    This is a python wrapper for the CellProfiler implementation of the following
    paper:

    R. Jonker and A. Volgenant, "A shortest augmenting path algorithm for
    dense and spare linear assignment problems", Computing, Vol. 38, pp.
    325-340, 1987.

    row_sol, cost, v, u, red_cost_mat = lpajv2.lapjv(cost_mat, resolution=None, calc_reduced_codt_matrix=False)

    :param cost_mat: cost matrix
    :type cost_mat: numpy array or python list
    :param resolution: this input argument is ignored. It is added only to expose the
    same signature as the python + numpy implementation.
    :type resolution: float
    :param calc_reduced_cost_matrix: set to true to calculate the reduced cost matrix
    :type calc_reduced_cost_matrix: bool
    :return: solution
    :rtype: tuple

    lapjv returns the optimal column indices *row_sol* assigned to each row
    and the minimum *cost* based on the assignment problem represented by
    the input cost matrix *cost_mat*.

    The (i,j)th element of *cost_mat* represents the cost to assign the
    jth job to the ith worker.

    @see lapjv_python
    """

    # Make sure we are working with a numpy array
    if type(cost_mat) is not np.ndarray:
        cost_mat = np.array(cost_mat)

    r_dim, c_dim = cost_mat.shape
    if r_dim > c_dim:
        r_dim, c_dim = c_dim, r_dim
        swap_f = True
    else:
        swap_f = False

    max_cost = np.max(cost_mat[cost_mat < np.Inf]) * c_dim + 1

    # Make sure the cost matrix is square
    if cost_mat.shape[1] > cost_mat.shape[0]:
        tmp = np.ones((cost_mat.shape[1] - cost_mat.shape[0], cost_mat.shape[1]), dtype=np.float32)
        tmp.fill(10 * max_cost)
        B = np.concatenate([cost_mat, tmp], axis=0)
        i_b, j_b = np.mgrid[0:B.shape[0], 0:B.shape[1]]
        i_b = i_b.flatten()
        j_b = j_b.flatten()
        x, y, u, v = lapjv.lapjv(i_b, j_b, B.flatten(), True, 0)
        x = x[0:cost_mat.shape[0]]
        u = u[0:cost_mat.shape[0]]
    elif cost_mat.shape[0] > cost_mat.shape[1]:
        tmp = np.ones((cost_mat.shape[0], cost_mat.shape[0] - cost_mat.shape[1]), dtype=np.float32)
        tmp.fill(10 * max_cost)
        B = np.concatenate([cost_mat, tmp], axis=1)
        B = B.T
        i_b, j_b = np.mgrid[0:B.shape[0], 0:B.shape[1]]
        i_b = i_b.flatten()
        j_b = j_b.flatten()
        x, y, u, v = lapjv.lapjv(i_b, j_b, B.flatten(), True, 0)
        x = x[0:cost_mat.shape[1]]
        u = u[0:cost_mat.shape[1]]
    else:
        i_b, j_b = np.mgrid[0:cost_mat.shape[0], 0:cost_mat.shape[1]]
        i_b = i_b.flatten()
        j_b = j_b.flatten()
        x, y, u, v = lapjv.lapjv(i_b, j_b, cost_mat.flatten(), True, 0)

    # Cost of the solution
    cost = np.sum(u) + np.sum(v[x])

    if calc_reduced_cost_matrix:

        # Reduced cost matrix
        cost_mat = cost_mat[0:r_dim, 0:c_dim] - \
                   np.kron(np.ones((1, c_dim)), u.reshape((len(u), 1))) - \
                   np.kron(np.ones((r_dim, 1)), v.reshape((1, len(v))))

        if swap_f:
            cost_mat = cost_mat.T

    if swap_f:
        u, v = v, u

    if cost > max_cost:
        cost = np.Inf

    return x, cost, v, u, cost_mat


def lapjv_python(cost_mat, resolution=None, calc_reduced_cost_matrix=False):
    """
    Jonker-Volgenant Algorithm for Linear Assignment Problem.

    This is a python + numpy port of the Matlab version by Yi Cao at Cranfield
    University, in turn developed based on the original C++ version coded by
    Roy Jonker at MagicLogic Optimization Inc on 4 September 1996.

    Reference:
    R. Jonker and A. Volgenant, "A shortest augmenting path algorithm for
    dense and spare linear assignment problems", Computing, Vol. 38, pp.
    325-340, 1987.

    row_sol, cost, v, u, red_cost_mat = lpajv2.lapjv(cost_mat, resolution=None)

    :param cost_mat: cost matrix
    :type cost_mat: numpy array or python list
    :param resolution: distance between cost to distinguish them
    :type resolution: float
    :param calc_reduced_cost_matrix: set to true to calculate the reduced cost matrix
    :type calc_reduced_cost_matrix: bool
    :return: solution
    :rtype: tuple

    lapjv returns the optimal column indices *row_sol* assigned to each row
    and the minimum *cost* based on the assignment problem represented by
    the input cost matrix *cost_mat*.

    The (i,j)th element of *cost_mat* represents the cost to assign the
    jth job to the ith worker.

    The second optional input, *resolution*, can be used to define the
    minimum difference between costs used to distinguish them and thus
    accelerate the solution. The original algorithm was developed for
    integer costs. When it is used for real (floating point) costs,
    sometime the algorithm  will take an extremely long time. In this
    case, using a reasonable large resolution as the second arguments
    can significantly increase the solution speed. The default resolution
    is `np.sqrt(np.finfo(np.float64).eps)`.

    For a rectangular (non-square) *cost_mat*, *row_sol* is the solution
    of the transposed cost matrix).

    Other output arguments are:
    v: dual variables, column reduction numbers.
    u: dual variables, row reduction numbers.
    red_cost_mat: the reduced cost matrix.

    :Example:

    >>> costs = [[17, 24, 1, 8, 15], [23, 5, 7, 14, 16], \
        [4, 6, 13, 20, 22], [10, 12, 19, 21, 3], [11, 18, 25, 2, 9]]
    >>> A, cost = lapjv_python(costs)
    >>> A
    [3, 2, 1, 5, 4]
    >>> cost
    15.0
    """

    # If no resolution is given, set it to sqrt(eps)
    if resolution is None:
        resolution = np.sqrt(np.finfo(np.float64).eps)

    # Make sure we are working with a numpy array
    if type(cost_mat) is not np.ndarray:
        cost_mat = np.array(cost_mat)

    # Prepare data
    M = np.min(cost_mat)
    r_dim, c_dim = cost_mat.shape

    if r_dim > c_dim:
        cost_mat = cost_mat.T
        r_dim, c_dim = c_dim, r_dim
        swap_f = True
    else:
        swap_f = False

    dim = c_dim

    cost_mat = np.row_stack((cost_mat, 2 * M + np.zeros((c_dim - r_dim, c_dim))))

    # This code from the MATLAB implementation is unclear to me
    cost_mat[cost_mat != cost_mat] = np.Inf
    if np.any(cost_mat < np.Inf):
        max_cost = np.max(cost_mat[cost_mat < np.Inf]) * dim + 1
    else:
        max_cost = np.Inf

    cost_mat[cost_mat == np.Inf] = max_cost

    # Dual variables, column reduction numbers
    v = np.zeros(dim)

    # Column assigned to row in solution
    row_sol = np.zeros(dim, dtype=np.int32) - 1

    # Row assigned to column in solution
    col_sol = np.zeros(dim, dtype=np.int32) - 1

    num_free = 0

    # List of unassigned rows
    free = -1 * np.ones(dim, dtype=np.int32)

    # Counts how many times a row could be assigned
    matches = np.zeros(dim, dtype=np.int32)

    #
    # The Initialization Phase
    #

    # Column reduction (reverse order gives better results)
    for j in range(dim - 1, -1, -1):

        # Find minimum cost over rows
        v[j] = np.min(cost_mat[:, j])
        i_min = np.where(cost_mat[:, j] == v[j])[0][0]

        if matches[i_min] == 0:

            # Init assignment if minimum row assigned is
            # for the first time
            row_sol[i_min] = j
            col_sol[j] = i_min

        elif v[j] < v[row_sol[i_min]]:

            j1 = int(row_sol[i_min])
            row_sol[i_min] = j
            col_sol[j] = i_min
            col_sol[j1] = -1

        else:

            # Row already assigned, column not assigned
            col_sol[j] = -1

        matches[i_min] += 1

    #
    # Reduction transfer from unassigned to assigned rows
    #

    for i in range(dim):

        if matches[i] == 0:

            # Fill list of unassigned 'free' rows
            free[num_free] = i
            num_free += 1

        else:

            if matches[i] == 1:
                # Transfer  reduction from rows that are assigned once
                j1 = row_sol[i]
                x = cost_mat[i, :] - v
                x[j1] = max_cost
                v[j1] -= np.min(x)

    #
    # Augmenting reduction of unassigned rows
    #

    loop_cnt = 0

    while loop_cnt < 2:

        loop_cnt += 1

        # Scan all free rows (in some cases, a free row may be replaced
        # with another one to be scanned next)
        k = -1
        prv_num_free = num_free - 1

        # Start list of rows still free after augmenting row reduction
        num_free = 0

        while k < prv_num_free:

            k += 1
            i = free[k]

            # Find minimum and second minimum reduced cost over columns
            x = cost_mat[i, :] - v.astype(np.float64)

            u_min = np.min(x)
            j1 = np.where(x == u_min)[0][0]
            x[j1] = max_cost

            u_sub_min = np.min(x)

            # Return the first if more than one
            j2 = np.where(x == u_sub_min)[0][0]

            i0 = col_sol[j1]

            if u_sub_min - u_min > resolution:

                # Change the reduction of the minimum column to increase the
                # minimum reduced cost in the row to the sub-minimum
                v[j1] -= u_sub_min - u_min

            else:

                # Minimum and sub-minimum equal
                if i0 > -1:
                    # Minimum column j1 is assigned.
                    # Swap columns j1 and j2, as j2 may be unassigned
                    j1 = j2
                    i0 = col_sol[j2]

            # Reassign i to j1, possibly de-assigning an i0
            row_sol[i] = j1
            col_sol[j1] = i

            if i0 > -1:

                # Minimum column j1 assigned easier
                if u_sub_min - u_min > resolution:

                    # Put in current k, and go back to that k
                    # Continue augmenting path i - j1 with i0.
                    free[k] = i0
                    k -= 1

                else:

                    # No further augmenting reduction possible
                    # Store i0 in list of free rows for next phase
                    free[num_free] = i0
                    num_free += 1

    #
    # Augmentation Phase
    #

    # Augment solution for each free row
    for f in range(num_free):

        # Start row of augmenting path
        free_row = free[f]

        # Dijkstra shortest path algorithm
        # Runs until unassigned column added to shortest path tree

        d = cost_mat[free_row, :] - v.astype(np.float64)
        pred = free_row * np.ones(dim, dtype=np.int32)

        col_list = np.arange(0, dim)

        # Columns in 1 ... (low - 1) are ready, now none
        low = 0

        # Columns in low ... (up - 1) are to be scanned for current minimum, now none
        up = 0

        # Columns in (up + 1) ...(dim - 1) are to be considered later to find new minimum,
        # at this stage the list simply contains all columns
        unassigned_found = False
        end_of_path = []
        while not unassigned_found:

            if up == low:

                # No more columns to be scanned for current minimum.
                last = low - 1

                # Scan columns for up ... (dim - 1) to find all indices for which new minimum occurs
                # Store these indices between (low + 1) ... up (increasing up).
                min_h = d[col_list[up]]
                up += 1

                for k in range(up, dim):

                    j = col_list[k]
                    h = d[j]
                    if h <= min_h:
                        if h < min_h:
                            up = low
                            min_h = h

                        # New index with same minimum, put on index up, and extend list.
                        col_list[k] = col_list[up]
                        col_list[up] = j
                        up += 1

                # Check if any of the minimum columns happens to be unassigned.
                # If so, we have an augmenting path right away.
                for k in range(low, up):

                    if col_sol[col_list[k]] < 0:
                        end_of_path = col_list[k]
                        unassigned_found = True
                        break

            if not unassigned_found:

                # Update 'distances' between free_row and all non-scanned columns,
                # via next scanned column
                j1 = col_list[low]
                low += 1
                i = col_sol[j1]
                x = cost_mat[i, :] - v
                h = x[j1] - min_h
                xh = x - h
                k = np.arange(up, dim)
                j = col_list[k]
                vf0 = xh < d
                vf = vf0[j]
                vj = j[vf]
                vk = k[vf]
                pred[vj] = i
                v2 = xh[vj]
                d[vj] = v2
                vf = v2 == min_h  # New column found at same minimum value
                j2 = vj[vf]
                k2 = vk[vf]
                cf = col_sol[j2] < 0

                # noinspection PyTypeChecker
                if np.any(cf):
                    # unassigned, shortest augmenting path is complete
                    i2 = cf.nonzero()[0][0]
                    end_of_path = j2[i2]
                    unassigned_found = True
                else:
                    # noinspection PyTypeChecker
                    i2 = len(cf)

                # Add to list to be scanned right away
                for k in range(i2 - 1):
                    col_list[k2[k]] = col_list[up]
                    col_list[up] = j2[k]
                    up += 1

        # Update column prices
        if last == -1:
            j1 = col_list[0]
        else:
            j1 = col_list[np.arange(0, last + 2)]
        v[j1] = v[j1] + d[j1] - min_h

        # Reset row and column assignments along the alternating path
        while True:
            i = pred[end_of_path]
            col_sol[end_of_path] = i
            j1 = end_of_path
            end_of_path = row_sol[i]
            row_sol[i] = j1
            if i == free_row:
                break

    row_sol = row_sol[0:r_dim]
    u = (np.diag(cost_mat[:, row_sol]) - v[row_sol]).T
    u = u[0:r_dim]
    v = v[0:c_dim]

    # Cost of the solution
    cost = np.sum(u) + np.sum(v[row_sol])

    if calc_reduced_cost_matrix:

        # Reduced cost matrix
        cost_mat = cost_mat[0:r_dim, 0:c_dim] - \
            np.kron(np.ones((1, c_dim)), u.reshape((len(u), 1))) - \
            np.kron(np.ones((r_dim, 1)), v.reshape((1, len(v))))

        if swap_f:
            cost_mat = cost_mat.T

    if swap_f:
        u, v = v, u

    if cost > max_cost:
        cost = np.Inf

    return row_sol, cost, v, u, cost_mat
