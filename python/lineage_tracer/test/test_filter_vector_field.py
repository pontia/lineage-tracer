'''test_filter_vector_field.py - Test the Gaussian weighted-averaging of a vector field.
'''

import unittest
import numpy as np
from lineage_tracer.analysis.filter import filter_vector_field


class TestFilterVectorField(unittest.TestCase):
    """
    Test filtering of a simple vector field.
    """

    def test_01_01_filter_arrays(self):
        '''Test simple filtering.'''

        # We have two vectors:
        #
        # Vector 1 at [0, 0] with components [x =  1.2, y =  1.4]
        # Vector 2 at [1, 1] with components [x =  0.3, y = -0.5]
        # Vector 3 at [2, 3] with components [x = -1.3, y =  2.0]
        #
        # Averaged with weights from a Gaussian bivariate PDF
        # with (supportX = 3.0 (sigma ~= 1.0), supportY = 3.0 (sigma ~= 1.0))
        # we expect the following result:
        #
        #    wdX = [0.95769491; 0.43835093; -1.17877361]
        #
        #    wdY = [0.89446993; 0.11677737; 1.81484707]

        # X coordinates of all vectors
        X = np.array([0,         # Vector 1's X coordinate
                      1,         # Vector 2's X coordinate
                      2])        # Vector 3's X coordinate

        Y = np.array([0,         # Vector 1's Y coordinate
                      1,         # Vector 2's Y coordinate
                      3])        # Vector 3's Y coordinate

        dX = np.array([1.2,      # Vector 1's X component
                       0.3,      # Vector 2's X component
                       -1.3])    # Vector 3's X component

        dY = np.array([1.4,      # Vector 1's Y component
                       -0.5,     # Vector 2's Y component
                       2.0])     # Vector 3's Y component

        support_radius_x = 3.0
        support_radius_y = 3.0

        wdX, wdY = filter_vector_field(X, Y, dX, dY, support_radius_x, support_radius_y)

        wdX_exp = np.array([0.95769491, 0.43835093, -1.17877361])
        wdY_exp = np.array([0.89446993, 0.11677737, 1.81484707])

        np.testing.assert_array_almost_equal(wdX, wdX_exp)
        np.testing.assert_array_almost_equal(wdY, wdY_exp)

    def test_01_02_filter_lists(self):
        '''Test simple filtering.'''

        # We have two vectors:
        #
        # Vector 1 at [0, 0] with components [x =  1.2, y =  1.4]
        # Vector 2 at [1, 1] with components [x =  0.3, y = -0.5]
        # Vector 3 at [2, 3] with components [x = -1.3, y =  2.0]
        #
        # Averaged with weights from a Gaussian bivariate PDF
        # with (supportX = 3.0 (sigma ~= 1.0), supportY = 3.0 (sigma ~= 1.0))
        # we expect the following result:
        #
        #    wdX = [0.95769491; 0.43835093; -1.17877361]
        #
        #    wdY = [0.89446993; 0.11677737; 1.81484707]

        # X coordinates of all vectors
        X = [0,  # Vector 1's X coordinate
             1,  # Vector 2's X coordinate
             2]  # Vector 3's X coordinate

        Y = [0,  # Vector 1's Y coordinate
             1,  # Vector 2's Y coordinate
             3]  # Vector 3's Y coordinate

        dX = [1.2,  # Vector 1's X component
              0.3,  # Vector 2's X component
             -1.3]  # Vector 3's X component

        dY = [1.4,  # Vector 1's Y component
             -0.5,  # Vector 2's Y component
              2.0]  # Vector 3's Y component

        support_radius_x = 3.0
        support_radius_y = 3.0

        wdX, wdY = filter_vector_field(X, Y, dX, dY, support_radius_x, support_radius_y)

        wdX_exp = np.array([0.95769491, 0.43835093, -1.17877361])
        wdY_exp = np.array([0.89446993, 0.11677737, 1.81484707])

        np.testing.assert_array_almost_equal(wdX, wdX_exp)
        np.testing.assert_array_almost_equal(wdY, wdY_exp)

    def test_01_03_filter_opposite(self):
        '''Test simple filtering.'''

        # We have two vectors:
        #
        # Vector 1 at [0, 0] with components [x =  -1.0, y =  0.0]
        # Vector 2 at [1, 0] with components [x =   1.0, y = 0.0]
        #
        # Averaged with weights from a Gaussian bivariate PDF
        # with (supportX = 3.0 (sigma ~= 1.0), supportY = 3.0 (sigma ~= 1.0))
        # we expect the following result:
        #
        #    wdX = [-0.247647007264, 0.247647007264]
        #
        #    wdY = [0.0, 0.0]

        # X coordinates of all vectors
        X = [0,  # Vector 1's X coordinate
             1]  # Vector 2's X coordinate

        Y = [0,  # Vector 1's Y coordinate
             0]  # Vector 2's Y coordinate

        dX = [-1.0,  # Vector 1's X component
              1.0]   # Vector 2's X component

        dY = [0.0,   # Vector 1's Y component
              0.0]   # Vector 2's Y component

        support_radius_x = 3.0
        support_radius_y = 3.0

        wdX, wdY = filter_vector_field(X, Y, dX, dY, support_radius_x, support_radius_y)

        wdX_exp = np.array([-0.247647007264, 0.247647007264])
        wdY_exp = np.array([0.0, 0.0])

        np.testing.assert_array_almost_equal(wdX, wdX_exp)
        np.testing.assert_array_almost_equal(wdY, wdY_exp)


    def test_01_04_filter_othogonal(self):
        '''Test simple filtering.'''

        # We have two vectors:
        #
        # Vector 1 at [0, 0] with components [x =  0.0, y = 1.0]
        # Vector 2 at [1, 0] with components [x =  1.0, y = 0.0]
        #
        # Averaged with weights from a Gaussian bivariate PDF
        # with (supportX = 3.0 (sigma ~= 1.0), supportY = 3.0 (sigma ~= 1.0))
        # we expect the following result:
        #
        #    wdX = [0.3761765, 0.6238235]
        #
        #    wdY = [0.6238235, 0.3761765]

        # X coordinates of all vectors
        X = [0,  # Vector 1's X coordinate
             1]  # Vector 2's X coordinate

        Y = [0,  # Vector 1's Y coordinate
             0]  # Vector 2's Y coordinate

        dX = [0.0,  # Vector 1's X component
              1.0]  # Vector 2's X component

        dY = [1.0,  # Vector 1's Y component
              0.0]  # Vector 2's Y component

        support_radius_x = 3.0
        support_radius_y = 3.0

        wdX, wdY = filter_vector_field(X, Y, dX, dY, support_radius_x, support_radius_y)

        wdX_exp = np.array([0.3761765, 0.6238235])
        wdY_exp = np.array([0.6238235, 0.3761765])

        np.testing.assert_array_almost_equal(wdX, wdX_exp)
        np.testing.assert_array_almost_equal(wdY, wdY_exp)
