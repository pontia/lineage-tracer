'''test_lapjv.py - test the Jonker-Volgenant implementation

CellProfiler is distributed under the GNU General Public License,
but this file is licensed under the more permissive BSD license.
See the accompanying file LICENSE for details.

Copyright (c) 2003-2009 Massachusetts Institute of Technology
Copyright (c) 2009-2014 Broad Institute
All rights reserved.
'''

import unittest

import numpy as np
from sympy.utilities.iterables import multiset_permutations
from lineage_tracer.lapjv import lapjv
from lineage_tracer.lapjv import lapjv2
from lineage_tracer.lapjv import _lapjv2


class TestLAPJVXvsMATLAB(unittest.TestCase):
    """
    Compare with the MATLAB implementation from
    http://ch.mathworks.com/matlabcentral/fileexchange/26836-lapjv-jonker-volgenant-algorithm-for-linear-assignment-problem-v3-0
    """

    def test_01_01_integer_cost(self):
        '''Test implementation with integer costs.'''
        costs = np.array(
            [[17, 24, 1, 8, 15],
             [23, 5, 7, 14, 16],
             [4, 6, 13, 20, 22],
             [10, 12, 19, 21, 3],
             [11, 18, 25, 2, 9]],
            dtype=np.uint8)
        x, cost, _, _, _ = lapjv2.lapjv_cython(costs)
        np.testing.assert_array_almost_equal(x, [2, 1, 0, 4, 3])
        np.testing.assert_array_almost_equal(cost,
                                             np.kron(np.ones((1, 5)), 15.0))

    def test_01_02_integer_cost_non_square(self):
        '''Test implementation with integer costs.

        To make the cost matrix square without affecting the
        solution, a column with the same large cost for all
        rows is added.
        '''
        costs = np.array(
            [[17, 24, 1, 8, 1000],
             [23, 5, 7, 14, 1000],
             [4, 6, 13, 20, 1000],
             [10, 12, 19, 21, 1000],
             [11, 18, 25, 2, 1000]],
            dtype=np.float32)
        x, cost, _, _, _ = lapjv2.lapjv_cython(costs)
        np.testing.assert_array_almost_equal(x, [2, 1, 0, 4, 3])
        np.testing.assert_array_almost_equal(cost,
                                             np.kron(np.ones((1, 5)), 1012.0))

    def test_01_03_float_cost(self):
        '''Test implementation with floating-value costs.'''
        costs = np.array(
            [[23.5000, 29.0000, 34.5000, 40.0000, 0.5000, 6.0000, 11.5000, 17.0000, 22.5000],
             [28.5000, 34.0000, 39.5000, 4.5000, 5.5000, 11.0000, 16.5000, 22.0000, 23.0000],
             [33.5000, 39.0000, 4.0000, 5.0000, 10.5000, 16.0000, 21.5000, 27.0000, 28.0000],
             [38.5000, 3.5000, 9.0000, 10.0000, 15.5000, 21.0000, 26.5000, 27.5000, 33.0000],
             [3.0000, 8.5000, 9.5000, 15.0000, 20.5000, 26.0000, 31.5000, 32.5000, 38.0000],
             [8.0000, 3.5000, 4.5000, 20.0000, 25.5000, 31.0000, 32.0000, 37.5000, 2.5000],
             [13.0000, 14.0000, 19.5000, 25.0000, 30.5000, 36.0000, 37.0000, 2.0000, 7.5000],
             [18.0000, 19.0000, 24.5000, 30.0000, 35.5000, 36.5000, 1.5000, 7.0000, 12.5000],
             [18.5000, 24.0000, 29.5000, 35.0000, 40.5000, 1.0000, 6.5000, 12.0000, 17.5000]],
            dtype=np.float32)
        x, cost, _, _, _ = lapjv2.lapjv_cython(costs)

        np.testing.assert_array_almost_equal(x, [4, 3, 2, 1, 0, 8, 7, 6, 5])
        np.testing.assert_array_almost_equal(cost,
                                             np.kron(np.ones((1, 9)), 22.5000))


class TestLAPJV2(unittest.TestCase):
    def test_01_00_standard_python_array(self):
        '''Test passing a standard python array as cost matrix.'''

        costs = [
            [17, 24, 1, 8, 15],
            [23, 5, 7, 14, 16],
            [4, 6, 13, 20, 22],
            [10, 12, 19, 21, 3]]

        res = lapjv2.lapjv_python(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [2, 1, 0, 4])
        np.testing.assert_array_almost_equal(cost, 13.0)

    def test_01_01_integer_non_square_non_augm(self):
        '''Test implementation with non-square integer matrix cost.'''
        costs = np.array([
            [17, 24, 1, 8, 15],
            [23, 5, 7, 14, 16],
            [4, 6, 13, 20, 22],
            [10, 12, 19, 21, 3]], dtype=np.float32)

        res = lapjv2.lapjv_python(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [2, 1, 0, 4])
        np.testing.assert_array_almost_equal(cost, 13.0)

    def test_01_02_float_non_square_non_augm(self):
        '''Test implementation with non-square floating-value matrix cost.'''
        costs = np.array(
            [[23.5000, 29.0000, 34.5000, 40.0000, 0.5000, 6.0000, 11.5000, 17.0000],
             [28.5000, 34.0000, 39.5000, 4.5000, 5.5000, 11.0000, 16.5000, 22.0000],
             [33.5000, 39.0000, 4.0000, 5.0000, 10.5000, 16.0000, 21.5000, 27.0000],
             [38.5000, 3.5000, 9.0000, 10.0000, 15.5000, 21.0000, 26.5000, 27.5000],
             [3.0000, 8.5000, 9.5000, 15.0000, 20.5000, 26.0000, 31.5000, 32.5000],
             [8.0000, 3.5000, 4.5000, 20.0000, 25.5000, 31.0000, 32.0000, 37.5000],
             [13.0000, 14.0000, 19.5000, 25.0000, 30.5000, 36.0000, 37.0000, 2.0000],
             [18.0000, 19.0000, 24.5000, 30.0000, 35.5000, 36.5000, 1.5000, 7.0000],
             [18.5000, 24.0000, 29.5000, 35.0000, 40.5000, 1.0000, 6.5000, 12.0000]],
            dtype=np.float32)

        res = lapjv2.lapjv_python(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [4, 3, 2, 1, 0, 8, 7, 6])
        np.testing.assert_array_almost_equal(cost, 20.0)

    def test_01_03_float_square_with_augm(self):
        '''Test implementation with augmentation.'''
        costs = np.array([
            [3, 5, 8, 6, 2, 4, 3, 1, 8, 7],
            [8, 4, 4, 6, 7, 3, 1, 6, 7, 2],
            [3, 8, 3, 7, 4, 6, 10, 9, 5, 2],
            [7, 6, 1, 3, 10, 7, 3, 7, 8, 1],
            [1, 8, 3, 2, 1, 3, 7, 10, 6, 9],
            [10, 10, 10, 9, 5, 7, 1, 9, 7, 10],
            [5, 9, 1, 10, 3, 10, 2, 9, 5, 9],
            [2, 9, 3, 4, 1, 2, 3, 8, 8, 6],
            [8, 4, 6, 7, 9, 9, 9, 4, 1, 8],
            [6, 9, 1, 4, 7, 5, 2, 7, 10, 2]], dtype=np.float32)

        res = lapjv2.lapjv_python(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [7, 1, 0, 9, 4, 6, 2, 5, 8, 3])
        np.testing.assert_array_almost_equal(cost, 19.0)

    def test_01_03_float_square_with_augm_complex(self):
        '''Test implementation with augmentation on a pathological case.'''
        costs = np.array([
            [1, 2, 3, 4],
            [1, 2, 3, 4],
            [1, 2, 3, 4]], dtype=np.float32)

        res = lapjv2.lapjv_python(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [0, 1, 2])
        np.testing.assert_array_almost_equal(cost, 6.0)

class Test_LAPJV2(unittest.TestCase):
    def test_01_00_non_square(self):
        ''''''

        costs = np.array([
            [17, 24, 1, 8, 15],
            [23, 5, 7, 14, 16],
            [4, 6, 13, 20, 22],
            [10, 12, 19, 21, 3]],
            dtype=np.float64)

        res = _lapjv2.lapjv(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [2, 1, 0, 4])
        np.testing.assert_array_almost_equal(cost, 13.0)

    def test_01_01_non_square_non_augm(self):
        '''Test implementation with non-square integer matrix cost.'''
        costs = np.array([
            [17, 24, 1, 8, 15],
            [23, 5, 7, 14, 16],
            [4, 6, 13, 20, 22],
            [10, 12, 19, 21, 3]], dtype=np.float64)

        res = _lapjv2.lapjv(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [2, 1, 0, 4])
        np.testing.assert_array_almost_equal(cost, 13.0)

    def test_01_02_float_non_square_non_augm(self):
        '''Test implementation with non-square floating-value matrix cost.'''
        costs = np.array(
            [[23.5000, 29.0000, 34.5000, 40.0000, 0.5000, 6.0000, 11.5000, 17.0000],
             [28.5000, 34.0000, 39.5000, 4.5000, 5.5000, 11.0000, 16.5000, 22.0000],
             [33.5000, 39.0000, 4.0000, 5.0000, 10.5000, 16.0000, 21.5000, 27.0000],
             [38.5000, 3.5000, 9.0000, 10.0000, 15.5000, 21.0000, 26.5000, 27.5000],
             [3.0000, 8.5000, 9.5000, 15.0000, 20.5000, 26.0000, 31.5000, 32.5000],
             [8.0000, 3.5000, 4.5000, 20.0000, 25.5000, 31.0000, 32.0000, 37.5000],
             [13.0000, 14.0000, 19.5000, 25.0000, 30.5000, 36.0000, 37.0000, 2.0000],
             [18.0000, 19.0000, 24.5000, 30.0000, 35.5000, 36.5000, 1.5000, 7.0000],
             [18.5000, 24.0000, 29.5000, 35.0000, 40.5000, 1.0000, 6.5000, 12.0000]],
            dtype=np.float64)

        res = _lapjv2.lapjv(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [4, 3, 2, 1, 0, 8, 7, 6])
        np.testing.assert_array_almost_equal(cost, 20.0)

    def test_01_03_float_square_with_augm(self):
        '''Test implementation with augmentation.'''
        costs = np.array([
            [3, 5, 8, 6, 2, 4, 3, 1, 8, 7],
            [8, 4, 4, 6, 7, 3, 1, 6, 7, 2],
            [3, 8, 3, 7, 4, 6, 10, 9, 5, 2],
            [7, 6, 1, 3, 10, 7, 3, 7, 8, 1],
            [1, 8, 3, 2, 1, 3, 7, 10, 6, 9],
            [10, 10, 10, 9, 5, 7, 1, 9, 7, 10],
            [5, 9, 1, 10, 3, 10, 2, 9, 5, 9],
            [2, 9, 3, 4, 1, 2, 3, 8, 8, 6],
            [8, 4, 6, 7, 9, 9, 9, 4, 1, 8],
            [6, 9, 1, 4, 7, 5, 2, 7, 10, 2]], dtype=np.float64)

        res = _lapjv2.lapjv(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [7, 1, 0, 9, 4, 6, 2, 5, 8, 3])
        np.testing.assert_array_almost_equal(cost, 19.0)

    def test_01_03_float_square_with_augm_complex(self):
        '''Test implementation with augmentation on a pathological case.'''
        costs = np.array([
            [1, 2, 3, 4],
            [1, 2, 3, 4],
            [1, 2, 3, 4]], dtype=np.float64)

        res = _lapjv2.lapjv(costs)
        sol = res[0]
        cost = res[1]

        np.testing.assert_array_almost_equal(sol, [0, 1, 2])
        np.testing.assert_array_almost_equal(cost, 6.0)


class TestLAPJVPYX(unittest.TestCase):
    def test_01_01_reduction_transfer(self):
        '''Test the reduction transfer implementation'''

        cases = [
            dict(i=[0, 1, 2],
                 j=[0, 1, 2, 0, 1, 2, 0, 1, 2],
                 idx=[0, 3, 6],
                 count=[3, 3, 3],
                 x=[2, 0, 1],
                 y=[1, 2, 0],
                 c=[5.0, 4.0, 1.0, 2.0, 6.0, 4.0, 4.0, 3.0, 7.0],
                 u_in=[0.0, 0.0, 0.0],
                 v_in=[1.0, 2.0, 3.0],
                 u_out=[2.0, 3.0, 6.0],
                 v_out=[-2.0, -4.0, 1.0]),
            dict(i=[1, 2, 3],
                 j=[0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2],
                 idx=[0, 3, 6, 9],
                 count=[3, 3, 3, 3],
                 x=[3, 2, 0, 1],
                 y=[1, 2, 0, 3],
                 c=[0.0, 0.0, 0.0, 5.0, 4.0, 1.0, 2.0, 6.0, 4.0, 4.0, 3.0, 7.0],
                 u_in=[0.0, 0.0, 0.0, 0.0],
                 v_in=[1.0, 2.0, 3.0, 0.0],
                 u_out=[0.0, 2.0, 3.0, 6.0],
                 v_out=[-2.0, -4.0, 1.0, 0.0]),
        ]
        for case in cases:
            u = np.ascontiguousarray(case["u_in"], np.float64)
            v = np.ascontiguousarray(case["v_in"], np.float64)
            lapjv.reduction_transfer(
                np.ascontiguousarray(case["i"], np.uint32),
                np.ascontiguousarray(case["j"], np.uint32),
                np.ascontiguousarray(case["idx"], np.uint32),
                np.ascontiguousarray(case["count"], np.uint32),
                np.ascontiguousarray(case["x"], np.uint32),
                u, v,
                np.ascontiguousarray(case["c"], np.float64))
            expected_u = np.array(case["u_out"])
            expected_v = np.array(case["v_out"])
            np.testing.assert_array_almost_equal(expected_u, u)
            np.testing.assert_array_almost_equal(expected_v, v)

    def test_02_01_augmenting_row_reduction(self):

        cases = [
            dict(n=3,
                 ii=[1],
                 jj=[0, 1, 2, 0, 1, 2, 0, 1, 2],
                 idx=[0, 3, 6],
                 count=[3, 3, 3],
                 x=[1, 3, 0],
                 y=[2, 0, 3],
                 u_in=[1.0, 2.0, 3.0],
                 v_in=[1.0, 2.0, 3.0],
                 c=[3.0, 6.0, 5.0, 5.0, 5.0, 7.1, 8.0, 11.0, 9.0],
                 u_out=[1.0, 2.0, 3.0],
                 v_out=[1.0, 1.0, 3.0],
                 x_out=[2, 1, 0],
                 y_out=[2, 1, 0])]
        for case in cases:
            u = np.ascontiguousarray(case["u_in"], np.float64)
            v = np.ascontiguousarray(case["v_in"], np.float64)
            x = np.ascontiguousarray(case["x"], np.uint32)
            y = np.ascontiguousarray(case["y"], np.uint32)
            lapjv.augmenting_row_reduction(
                case["n"],
                np.ascontiguousarray(case["ii"], np.uint32),
                np.ascontiguousarray(case["jj"], np.uint32),
                np.ascontiguousarray(case["idx"], np.uint32),
                np.ascontiguousarray(case["count"], np.uint32),
                x, y, u, v,
                np.ascontiguousarray(case["c"], np.float64))
            expected_u = np.array(case["u_out"])
            expected_v = np.array(case["v_out"])
            expected_x = np.array(case["x_out"])
            expected_y = np.array(case["y_out"])
            np.testing.assert_array_almost_equal(expected_u, u)
            np.testing.assert_array_almost_equal(expected_v, v)
            np.testing.assert_array_equal(expected_x, x)
            np.testing.assert_array_equal(expected_y, y)

    def test_03_01_augment(self):
        cases = [
            dict(n=3,
                 i=[2],
                 j=[0, 1, 2, 0, 1, 2, 0, 1, 2],
                 idx=[0, 3, 6],
                 count=[3, 3, 3],
                 x_in=[0, 1, 3],
                 x_out=[0, 1, 2],
                 y_in=[0, 1, 3],
                 y_out=[0, 1, 2],
                 u_in=[4, 0, 2],
                 v_in=[-1, 1, 1],
                 u_out=[4, 0, 2],
                 v_out=[-1, 1, 1],
                 c=[3, 5, 7, 4, 1, 6, 2, 3, 3])]
        for case in cases:
            n = case["n"]
            i = np.ascontiguousarray(case["i"], np.uint32)
            j = np.ascontiguousarray(case["j"], np.uint32)
            idx = np.ascontiguousarray(case["idx"], np.uint32)
            count = np.ascontiguousarray(case["count"], np.uint32)
            x = np.ascontiguousarray(case["x_in"], np.uint32)
            y = np.ascontiguousarray(case["y_in"], np.uint32)
            u = np.ascontiguousarray(case["u_in"], np.float64)
            v = np.ascontiguousarray(case["v_in"], np.float64)
            c = np.ascontiguousarray(case["c"], np.float64)
            lapjv.augment(n, i, j, idx, count, x, y, u, v, c)
            np.testing.assert_array_equal(x, case["x_out"])
            np.testing.assert_array_equal(y, case["y_out"])
            np.testing.assert_almost_equal(u, case["u_out"])
            np.testing.assert_almost_equal(v, case["v_out"])


class TestLAPJV(unittest.TestCase):
    def test_01_02(self):
        r = np.random.RandomState()
        r.seed(11)
        for reductions in [0, 2]:
            for _ in range(100):
                c = r.randint(1, 10, (5, 5))
                i, j = np.mgrid[0:5, 0:5]
                i = i.flatten()
                j = j.flatten()
                x, y, u, v = lapjv.lapjv(i, j, c.flatten(), True, reductions)
                min_cost = np.sum(c)
                best = None
                for permutation in multiset_permutations([0, 1, 2, 3, 4]):
                    cost = sum([c[i, permutation[i]] for i in range(5)])
                    if cost < min_cost:
                        best = list(permutation)
                        min_cost = cost
                result_cost = sum([c[i, x[i]] for i in range(5)])
                self.assertAlmostEqual(min_cost, result_cost)

    def test_01_03(self):
        '''Regression test of matrices that crashed lapjv'''
        dd = [
            np.array([[0., 0., 0.],
                      [1., 1., 5.34621029],
                      [1., 7., 55.],
                      [2., 2., 2.09806089],
                      [2., 8., 55.],
                      [3., 3., 4.82063029],
                      [3., 9., 55.],
                      [4., 4., 3.99481917],
                      [4., 10., 55.],
                      [5., 5., 3.18959054],
                      [5., 11., 55.],
                      [6., 1., 55.],
                      [6., 7., 0.],
                      [6., 8., 0.],
                      [6., 9., 0.],
                      [6., 10., 0.],
                      [6., 11., 0.],
                      [7., 2., 55.],
                      [7., 7., 0.],
                      [7., 8., 0.],
                      [7., 9., 0.],
                      [7., 10., 0.],
                      [7., 11., 0.],
                      [8., 3., 55.],
                      [8., 7., 0.],
                      [8., 8., 0.],
                      [8., 9., 0.],
                      [8., 10., 0.],
                      [8., 11., 0.],
                      [9., 4., 55.],
                      [9., 7., 0.],
                      [9., 8., 0.],
                      [9., 9., 0.],
                      [9., 10., 0.],
                      [9., 11., 0.],
                      [10., 5., 55.],
                      [10., 7., 0.],
                      [10., 8., 0.],
                      [10., 9., 0.],
                      [10., 10., 0.],
                      [10., 11., 0.],
                      [11., 6., 55.],
                      [11., 7., 0.],
                      [11., 8., 0.],
                      [11., 9., 0.],
                      [11., 10., 0.],
                      [11., 11., 0.]]),
            np.array([[0., 0., 0.],
                      [1., 1., 1.12227977],
                      [1., 6., 55.],
                      [2., 2., 18.66735253],
                      [2., 4., 16.2875504],
                      [2., 7., 55.],
                      [3., 5., 1.29944194],
                      [3., 8., 55.],
                      [4., 5., 32.61892281],
                      [4., 9., 55.],
                      [5., 1., 55.],
                      [5., 6., 0.],
                      [5., 7., 0.],
                      [5., 8., 0.],
                      [5., 9., 0.],
                      [6., 2., 55.],
                      [6., 6., 0.],
                      [6., 7., 0.],
                      [6., 8., 0.],
                      [6., 9., 0.],
                      [7., 3., 55.],
                      [7., 6., 0.],
                      [7., 7., 0.],
                      [7., 8., 0.],
                      [7., 9., 0.],
                      [8., 4., 55.],
                      [8., 6., 0.],
                      [8., 7., 0.],
                      [8., 8., 0.],
                      [8., 9., 0.],
                      [9., 5., 55.],
                      [9., 6., 0.],
                      [9., 7., 0.],
                      [9., 8., 0.],
                      [9., 9., 0.]])]
        expected_costs = [74.5, 1000000]
        for d, ec in zip(dd, expected_costs):
            n = np.max(d[:, 0].astype(int)) + 1
            x, y = lapjv.lapjv(d[:, 0].astype(int), d[:, 1].astype(int), d[:, 2])
            c = np.ones((n, n)) * 1000000
            c[d[:, 0].astype(int), d[:, 1].astype(int)] = d[:, 2]
            self.assertTrue(np.sum(c[np.arange(n), x]) < ec)
            self.assertTrue(np.sum(c[y, np.arange(n)]) < ec)


class TestLAPJVXvsLAPJV2x(unittest.TestCase):
    def test_01_01_large(self):
        import random
        import time

        N = 1000

        # Initialize cost matrix
        random.seed(101)
        costs = np.random.randint(0, 100000, (N, N))

        # Solution with lapjv2
        t = time.time()
        res = lapjv2.lapjv_python(costs)
        elapsed = time.time() - t
        print('LAPVJ2: elapsed time ' + str(elapsed) + 's')

        # Solution with _lapjv2
        t = time.time()
        res = _lapjv2.lapjv(costs.astype(np.float64))
        elapsed = time.time() - t
        print('_LAPVJ2: elapsed time ' + str(elapsed) + 's')

        sol = res[0]
        cost2 = res[1]

        # Solution with lapjv
        t = time.time()
        x, cost, _, _, _ = lapjv2.lapjv_cython(costs)
        elapsed = time.time() - t
        print('LAPVJ: elapsed time ' + str(elapsed) + 's')

        # cost = np.sum(u) + np.sum(v[x])

        # Compare the results
        np.testing.assert_array_almost_equal(sol, x)
        np.testing.assert_array_almost_equal(cost2, cost)


    def test_01_02_integer_cost(self):
        '''Test implementation with integer costs.'''
        costs = np.array(
            [[17, 24, 1, 8, 15],
             [23, 5, 7, 14, 16],
             [4, 6, 13, 20, 22],
             [10, 12, 19, 21, 3],
             [11, 18, 25, 2, 9]],
            dtype=np.float64)
        x, cost, _, _, _ = _lapjv2.lapjv(costs.astype(np.float64))
        np.testing.assert_array_almost_equal(x, [2, 1, 0, 4, 3])
        np.testing.assert_array_almost_equal(cost,
                                             np.kron(np.ones((1, 5)), 15.0))


    def test_01_03_larger(self):
        '''Test implementation with integer costs.'''
        costs = np.array(
            [[23.5000, 29.0000, 34.5000, 40.0000, 0.5000, 6.0000, 11.5000, 17.0000, 22.5000],
             [28.5000, 34.0000, 39.5000, 4.5000, 5.5000, 11.0000, 16.5000, 22.0000, 23.0000],
             [33.5000, 39.0000, 4.0000, 5.0000, 10.5000, 16.0000, 21.5000, 27.0000, 28.0000],
             [38.5000, 3.5000, 9.0000, 10.0000, 15.5000, 21.0000, 26.5000, 27.5000, 33.0000],
             [3.0000, 8.5000, 9.5000, 15.0000, 20.5000, 26.0000, 31.5000, 32.5000, 38.0000],
             [8.0000, 3.5000, 4.5000, 20.0000, 25.5000, 31.0000, 32.0000, 37.5000, 2.5000],
             [13.0000, 14.0000, 19.5000, 25.0000, 30.5000, 36.0000, 37.0000, 2.0000, 7.5000],
             [18.0000, 19.0000, 24.5000, 30.0000, 35.5000, 36.5000, 1.5000, 7.0000, 12.5000],
             [18.5000, 24.0000, 29.5000, 35.0000, 40.5000, 1.0000, 6.5000, 12.0000, 17.5000]],
            dtype=np.float64)
        x, cost, _, _, _ = _lapjv2.lapjv(costs)
        np.testing.assert_array_almost_equal(x, [4, 3, 2, 1, 0, 8, 7, 6, 5])
        np.testing.assert_array_almost_equal(cost,
                                             np.kron(np.ones((1, 9)), 22.5000))
