'''test_tracker.py - Test the actual tracker (comparisons are done with the
results of the original MATLAB tracker).
'''
import glob
import unittest
import os
import pandas as pd
import numpy as np

from lineage_tracer.analysis.tracker import Tracker
from lineage_tracer.tools.fio import sort_file_list


class TestTracker(unittest.TestCase):

    def setUp(self):

        self.currDir = os.path.dirname(
            os.path.realpath(__file__).replace("\\", "/"))

    def test_01_01_plain_tracker(self):
        """
        Test running the tracker without quality control.
        """

        # Paths to dataset 1

        # Retrieve the file list
        file_list, _ = sort_file_list(
            glob.glob(os.path.join(
                self.currDir, 'data', 'dataset_01', 'cells*.txt')))

        # Initialize the tracker - we do not need the images if we do not
        # perform the quality control
        tracker = Tracker(file_list=file_list,
                          image_file_list=[],
                          result_folder='',
                          factor_area=5000,
                          factor_orientation=0,
                          displacement_penalty=2,
                          growth_penalty=0.03,
                          jump_penalty=200,
                          max_penalty=2000,
                          drift=0.7,
                          max_jump=2,
                          filter_support=0,
                          export=False,
                          qc=False)

        # Run the tracker
        tracker.track()

        # Get the MATLAB results
        matlab_file_list, _ = sort_file_list(
            glob.glob(os.path.join(
                self.currDir, 'data', 'dataset_01', 'matlab', 'cells*.txt')))

        # Now compare the results
        for i in range(tracker.num_data_frames()):

            # Get the result (from memory)
            python_df = tracker.load_dataframe_for_index(i)

            # Read the MATLAB result file
            matlab_df = pd.read_csv(matlab_file_list[i],
                                    header=None,
                                    sep='\t')

            # Track indices (track indices in python are 0-based)
            python_track_indices = python_df['track.index'].values + 1
            matlab_track_indices = matlab_df[0].values

            self.assertTrue(np.array_equal(python_track_indices,
                                           matlab_track_indices))

            if 'cost' in python_df.columns.values:

                # Total cost
                python_cost = python_df['cost'].values
                matlab_cost = matlab_df[1].values
                self.assertTrue(np.all(
                    np.isclose(
                        python_cost,
                        matlab_cost,
                        atol=1e-06,
                        equal_nan=True)))

                # Cost 1
                python_cost_1 = python_df['cost1'].values
                matlab_cost_1 = matlab_df[2].values
                self.assertTrue(np.all(
                    np.isclose(
                        python_cost_1,
                        matlab_cost_1,
                        atol=1e-06,
                        equal_nan=True)))

                # Cost 2
                python_cost_2 = python_df['cost2'].values
                matlab_cost_2 = matlab_df[3].values
                self.assertTrue(np.all(
                    np.isclose(
                        python_cost_2,
                        matlab_cost_2,
                        atol=1e-06,
                        equal_nan=True)))

                # Cost 3
                python_cost_3 = python_df['cost3'].values
                matlab_cost_3 = matlab_df[4].values
                self.assertTrue(np.all(
                    np.isclose(
                        python_cost_3,
                        matlab_cost_3,
                        atol=1e-06,
                        equal_nan=True)))

                # Cost 4
                python_cost_4 = python_df['cost4'].values
                matlab_cost_4 = matlab_df[5].values
                self.assertTrue(np.all(
                    np.isclose(
                        python_cost_4,
                        matlab_cost_4,
                        atol=1e-06,
                        equal_nan=True)))

    def test_01_02_tracker_with_filtering(self):
        """
        Test running the tracker without quality control but with filtering.
        """

        # Paths to dataset 1

        # Retrieve the file list
        file_list, _ = sort_file_list(
            glob.glob(os.path.join(
                self.currDir, 'data', 'dataset_01', 'cells*.txt')))

        # Initialize the tracker - we do not need the images if we do not
        # perform the quality control
        tracker = Tracker(file_list=file_list,
                          image_file_list=[],
                          result_folder='',
                          factor_area=5000,
                          factor_orientation=0,
                          displacement_penalty=2,
                          growth_penalty=0.03,
                          jump_penalty=200,
                          max_penalty=2000,
                          drift=0.7,
                          max_jump=2,
                          filter_support=100,
                          export=False,
                          qc=False)

        # Run the tracker
        tracker.track()

        # Get the MATLAB results
        matlab_file_list, _ = sort_file_list(
            glob.glob(os.path.join(
                self.currDir, 'data', 'dataset_01', 'matlab', 'cells*.txt')))

        # We just compare the track indices
        for i in range(tracker.num_data_frames()):

            # Get the result (from memory)
            python_df = tracker.load_dataframe_for_index(i)

            # Read the MATLAB result file
            matlab_df = pd.read_csv(matlab_file_list[i],
                                    header=None,
                                    sep='\t')

            # Track indices (track indices in python are 0-based)
            python_track_indices = python_df['track.index'].values + 1
            matlab_track_indices = matlab_df[0].values

            self.assertTrue(np.array_equal(python_track_indices,
                                           matlab_track_indices))

    def test_01_03_plain_tracker_pass_settings(self):

        # Initialize the tracker
        tracker = Tracker()

        # Override default settings
        settings = tracker.settings
        settings['factor_area'] = 5000
        settings['factor_orientation'] = 0
        settings['displacement_penalty'] = 2
        settings['growth_penalty'] = 0.03
        settings['jump_penalty'] = 200
        settings['max_penalty'] = 2000
        settings['drift'] = 0.7
        settings['filter_support'] = 0
        settings['export'] = False
        settings['qc'] = False

        pass

    def tearDown(self):

        try:
            os.remove(os.path.join(
                self.currDir, './project.h5'))
        except:
            print("Could not delete temporary project file.")


if __name__ == '__main__':
    unittest.main()
